#!/usr/bin/env python2

import PIL.Image as Image, PIL.ImageDraw as ImageDraw
import sys
from decimal import Decimal

# file format is any number of lines of:
# l t r b
# the coordinates have the same semantics as RECT structs in GDI
# the resulting image will be a black background with the rectangles
# drawn in white

if len(sys.argv) != 3:
    print "Usage:", sys.argv[0], "<rects file>", "<output file>"
    exit(1)

boxes = []
with open(sys.argv[1]) as f:
    for line in f:
        strs = line.split(" ")
        boxes.append((int(Decimal(strs[0])), int(Decimal(strs[1])), int(Decimal(strs[2])), int(Decimal(strs[3]))))

maxX = maxY = 0
for box in boxes:
    if box[2] > maxX:
        maxX = box[2]
    if box[3] > maxY:
        maxY = box[3]

img = Image.new("RGB", (maxX, maxY))
draw = ImageDraw.Draw(img)

fill=0x1F
for box in boxes:
    draw.rectangle(box, fill=fill)
    fill = fill + 0x1f

img.save(sys.argv[2])
