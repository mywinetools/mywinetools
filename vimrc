set tabstop=8
set shiftwidth=4
set expandtab
set nowrap
set mouse=
set viminfo='1000,\"500,%
set ignorecase
set list listchars=tab:»·,trail:·,nbsp:␣
set scrolloff=5

let g:is_posix = 1

au BufNewFile,BufRead *.git/COMMIT_EDITMSG set tw=72 noai noshowmatch
au BufNewFile,BufRead *.git/COMMIT_EDITMSG setlocal spell spelllang=en_us

source ~/mywinetools/implement_com.vim
