[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '

complete -r

export PATH="$HOME/bin:/home/aeikum/.local/bin:$PATH"
export HISTCONTROL=ignoredups
export EDITOR=vim
export VISUAL="$EDITOR"
export MAKEFLAGS="-j6"
export LESS="-RXi"
export ALL_AUDIO_DEBUG="+timestamp,+tid,+mmdevapi,+winmm,+driver,+midi,+dsound,+dsound3d,+dmusic,+mci,+oss,+alsa,+coreaudio,+pulse,+dmime,+dmloader,+dmfile,+dmfileraw,+dmdump,+dmband,+dmusic,+dmcompos,+dmscript,+dmstyle,+dmsynth,+dmusic32,+dswave,+mciqtz,+quartz,+mciwave,+seh,+xaudio2,+msacm,+adpcm,+gstreamer,+wavemap,+devenum"
export ALL_GST_DEBUG="$ALL_AUDIO_DEBUG,+quartz,+strmbase,+strmbase_qc,+gstreamer,+qtdecoder,+qtsplitter,+qtdatahandler,+amstream,+qcap,+qcap_v4l,+qedit,+mciqtz,+mci"
export PAGER="less"
export MANWIDTH=80
#export WINEDLLOVERRIDES="winemenubuilder.exe=d"
#export WINE=$HOME/wine/wine
alias ls='ls --color=auto'
alias l='ls'
alias cp='cp -i'
alias md5=md5sum
alias vmi='vim'
alias ivm='vim'
alias vi='vim'
alias abs="echo you want, 'asp checkout <pkgname>'"
alias grep='LANG=C grep -d skip -IHin --exclude=*~'
alias grpe='grep'
alias sr='screen -raAd'
alias amke='make'
alias mkae='make'
alias maek='make'
alias m1='make -j1'
alias mt="m1 2>&1 | head -n20"
alias bzip2='pbzip2 -k'
alias bunzip2='pbunzip2 -k'
alias qgit='git'
alias vimr='vim -R'
alias ps-e='ps -e'

man() {
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[1;31m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;32m") \
            command man "$@"
}
