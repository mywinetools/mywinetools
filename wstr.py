#!/usr/bin/env python3

import sys

def usage():
    print("Usage:")
    print("\t" + sys.argv[0] + " <string(s)>")

if len(sys.argv) < 2:
    usage()
else:
    for word in sys.argv[1:]:
        sys.stdout.write("{")
        for char in word:
            if char == '\'':
                sys.stdout.write("'\\'',")
            elif char == '\\':
                sys.stdout.write("'\\\\',")
            else:
                #sys.stdout.write(str(int(ord(char))) + ", ")
                sys.stdout.write('\'' + char + "\',")
        sys.stdout.write("0};\n")
