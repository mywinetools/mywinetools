#!/usr/bin/env python2

import re
import sys

regex = '([0-9a-fA-F]{4}:(?:Ret|Call|RET|CALL))'
matcher = re.compile(regex)

threads = dict()

def parse_line(a_line, num):
    chunks = matcher.split(a_line)
    if len(chunks) > 3:
        print "[" + str(num) + "] Warning: multithreaded guesswork"
    skip = False
    for i in range(len(chunks)):
        if i % 2 == 0:
            if skip == True:
                skip = False
                continue
            if chunks[i] != '':
                out.write(chunks[i])
                if not chunks[i].endswith('\n'):
                    out.write('\n')
        else:
            thread = chunks[i][:4]
            if ((len(good) > 0) and (not thread in good)) or \
                    ((len(bad) > 0) and (thread in bad)):
                skip = True
                continue
            if thread in threads:
                indent = threads[thread]
            else:
                indent = 0
            if chunks[i][5:8] == "Ret" or chunks[i][5:8] == "RET":
                indent = indent - 1
                if indent < 0:
                    print "[" + str(num) + "] Warning: negative indent level on thread " + thread
                    indent = 0
                threads[thread] = indent
            out.write("  " * indent)
            out.write(chunks[i])
            if chunks[i][5:8] == "Cal" or chunks[i][5:8] == "CAL":
                #exceptions never return, so don't indent
                if chunks[i + 1].find("KERNEL32.RaiseException") == -1 and \
                        chunks[i + 1].find("ucrtbase._CxxThrowException") == -1:
                    threads[thread] = indent + 1

def usage():
    print "Usage:"
    print "\t" + sys.argv[0] + " <input name> <output name> [threads]"

if len(sys.argv) < 3:
    usage()
elif sys.argv[1] == sys.argv[2]:
    print "Error: Input & output filenames cannot be the same!"
    usage()
else:
    good = []
    bad = []
    for i in range(3, len(sys.argv)):
        if sys.argv[i][0] == '-':
            if good.count(sys.argv[i][1:]) > 0:
                good.remove(sys.argv[i][1:])
            bad.append(sys.argv[i][1:])
        elif sys.argv[i][0] == '+':
            if bad.count(sys.argv[i][1:]) > 0:
                bad.remove(sys.argv[i][1:])
            good.append(sys.argv[i][1:])
        else:
            good.append(sys.argv[i])

    if len(good) > 0:
        print("Ignoring all threads except " + str(good))
    elif len(bad) > 0:
        print("Using all threads except " + str(bad))
    else:
        print("Ignoring no threads")

    with open(sys.argv[2], "w") as out:
        count = 0
        with open(sys.argv[1], "r") as f:
            for line in f:
                count = count + 1
                parse_line(line, count)
