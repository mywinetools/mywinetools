#include <stdio.h>
#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windows.h"
#define COBJMACROS
#include "initguid.h"
#include "dxgi.h"
#include <stdarg.h>
#include <stdint.h>

static FILE *f;

static const char *wtoa(const WCHAR *w)
{
    static char a[4096];
    WideCharToMultiByte(CP_UTF8, 0, w, -1, a, sizeof(a), NULL, NULL);
    return a;
}

static void test_output(IDXGIOutput *output)
{
    HRESULT hr;
    DXGI_OUTPUT_DESC desc;
    UINT count, i;
    DXGI_MODE_DESC *modes;

    memset(&desc, 0, sizeof(desc));

    hr = IDXGIOutput_GetDesc(output, &desc);
    if(hr == S_OK)
    {
        fprintf(f, "got output.DeviceName: %s\n", wtoa(desc.DeviceName));
        fprintf(f, "got output.DesktopCoords: %d,%d , %d,%d\n",
                desc.DesktopCoordinates.left,
                desc.DesktopCoordinates.top,
                desc.DesktopCoordinates.right,
                desc.DesktopCoordinates.bottom);
        fprintf(f, "got output.AttachedToDesktop: %u\n",
                desc.AttachedToDesktop);
        fprintf(f, "got output.Rotation: 0x%x\n", desc.Rotation);
        fprintf(f, "got output.Monitor: 0x%x\n", desc.Monitor);
    }else
        fprintf(f, "Output_GetDesc failed: %#x\n", hr);

    hr = IDXGIOutput_GetDisplayModeList(output, DXGI_FORMAT_R8G8B8A8_UNORM_SRGB, 0, &count, NULL);
    if(hr != S_OK)
        fprintf(f, "GetDisplayModeList(len) failed: %#x\n", hr);

    modes = HeapAlloc(GetProcessHeap(), 0, sizeof(*modes) * count);

    hr = IDXGIOutput_GetDisplayModeList(output, DXGI_FORMAT_R8G8B8A8_UNORM_SRGB, 0, &count, modes);
    if(hr != S_OK)
        fprintf(f, "GetDisplayModeList(data) failed: %#x\n", hr);

    for(i = 0; i < count; ++i)
    {
        fprintf(f, "got mode[%u].Width: %u\n", i, modes[i].Width);
        fprintf(f, "got mode[%u].Height: %u\n", i, modes[i].Height);
        fprintf(f, "got mode[%u].RefreshRate: %u / %u\n", i, modes[i].RefreshRate.Numerator,
                modes[i].RefreshRate.Denominator);
        fprintf(f, "got mode[%u].Format: 0x%x\n", i, modes[i].Format);
        fprintf(f, "got mode[%u].ScanlineOrdering: 0x%x\n", i, modes[i].ScanlineOrdering);
        fprintf(f, "got mode[%u].Scaling: 0x%x\n", i, modes[i].Scaling);
    }

    HeapFree(GetProcessHeap(), 0, modes);
}

static void test_adapter(IDXGIAdapter1 *adapter)
{
    HRESULT hr;
    DXGI_ADAPTER_DESC1 desc;
    UINT output_idx;
    IDXGIOutput *output;

    memset(&desc, 0, sizeof(desc));
    hr = IDXGIAdapter1_GetDesc1(adapter, &desc);

    if(hr == S_OK)
    {
        fprintf(f, "got desc.Description: %s\n", wtoa(desc.Description));
        fprintf(f, "got desc.VendorId: 0x%x\n", desc.VendorId);
        fprintf(f, "got desc.DeviceId: 0x%x\n", desc.DeviceId);
        fprintf(f, "got desc.SubSysId: 0x%x\n", desc.SubSysId);
        fprintf(f, "got desc.Revision : 0x%x\n", desc.Revision);
        fprintf(f, "got desc.VidMem: 0x%x\n", desc.DedicatedVideoMemory);
        fprintf(f, "got desc.SysMem: 0x%x\n", desc.DedicatedSystemMemory);
        fprintf(f, "got desc.SharedMem: 0x%x\n", desc.SharedSystemMemory);
        fprintf(f, "got desc.AdapterLuid: {0x%x, 0x%x}\n", desc.AdapterLuid.HighPart, desc.AdapterLuid.LowPart);
        fprintf(f, "got desc.Flags: 0x%#x\n", desc.AdapterLuid);
    }else
        fprintf(f, "Adapter1_GetDesc1 failed: %#x\n", hr);

    output_idx = 0;
    while(1)
    {
        hr = IDXGIAdapter1_EnumOutputs(adapter, output_idx, &output);
        if(hr == S_OK)
        {
            fprintf(f, "testing output %u\n", output_idx);
            test_output(output);
            IDXGIOutput_Release(output);
        }
        else
            break;

        output_idx++;
    }
}

static void test_my(void)
{
    IDXGIFactory1 *fact;
    HRESULT hr;
    UINT adapter_idx;
    IDXGIAdapter1 *adapter;

    hr = CreateDXGIFactory1(&IID_IDXGIFactory1, (void**)&fact);
    if(hr != S_OK)
    {
        fprintf(f, "CreateDXGIFactory1 failed: %#x\n", hr);
        return;
    }

    adapter_idx = 0;
    while(1)
    {
        hr = IDXGIFactory1_EnumAdapters1(fact, adapter_idx, &adapter);
        if(hr == S_OK)
        {
            fprintf(f, "testing adapter %u\n", adapter_idx);
            test_adapter(adapter);
            fprintf(f, "------\n");
            IDXGIAdapter1_Release(adapter);
        }else
            break;

        adapter_idx++;
    }
}

int main(int argc, char **argv)
{
    if(argc > 1)
        f = fopen(argv[1], "w");
    else
        f = stdout;

    test_my();
    fclose(f);
    return 0;
}
