#define COBJMACROS
#include <initguid.h>
#include <dsound.h>
#include <stdio.h>
#include <uuids.h>
#include <ksmedia.h>
#include <mmdeviceapi.h>
#include <mmsystem.h>
#include <audioclient.h>

static IDirectSound *ds;
static FILE *f;

static GUID float_subtype = {
0x00000003, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71}
};

static GUID pcm_subtype = {
0x00000001, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71}
};

static const unsigned char six_chan_dat[] = {
#include "six_chan_wav.h"
};

static void check_mmdev(void)
{
    HRESULT hr;
    IMMDeviceEnumerator *mme;
    IMMDevice *dev;
    IAudioClient *ac;
    WAVEFORMATEX *pwfx;

    CoInitializeEx(NULL, COINIT_MULTITHREADED);

    hr = CoCreateInstance(&CLSID_MMDeviceEnumerator, NULL, CLSCTX_INPROC_SERVER,
            &IID_IMMDeviceEnumerator, (void**)&mme);
    if(FAILED(hr))
    {
        fprintf(f, "CCI failed: %08lx\n", hr);
        return;
    }

    hr = IMMDeviceEnumerator_GetDefaultAudioEndpoint(mme, eRender, eMultimedia, &dev);
    if(FAILED(hr))
    {
        fprintf(f, "GDAE failed: %08lx\n", hr);
        return;
    }

    hr = IMMDevice_Activate(dev, &IID_IAudioClient, CLSCTX_INPROC_SERVER, NULL, (void**)&ac);
    if(FAILED(hr))
    {
        fprintf(f, "A failed: %08lx\n", hr);
        return;
    }

    hr = IAudioClient_GetMixFormat(ac, &pwfx);
    if(FAILED(hr))
    {
        fprintf(f, "GMF failed: %08lx\n", hr);
        return;
    }

    fprintf(f, "mmdev mix format chans: %u\n", pwfx->nChannels);

    CoTaskMemFree(pwfx);

    IAudioClient_Release(ac);
    IMMDevice_Release(dev);
    IMMDeviceEnumerator_Release(mme);
}

int main(int argc, char **argv)
{
    HRESULT hr;
    DSBUFFERDESC desc;
    IDirectSoundBuffer *primary, *secondary;
    DWORD fmt_sz;
    WAVEFORMATEX *fmtex;
    WAVEFORMATEXTENSIBLE sec_fmt;
    void *audio1, *audio2;
    DWORD audio1_len, audio2_len;
    int skip_primary = 0;

    if(argc > 1)
    {
        if(*argv[1] == '-')
        {
            skip_primary = 1;
            if(argc > 2)
                f = fopen(argv[2], "w");
        }else
            f = fopen(argv[1], "w");
    }

    if(!f)
        f = stdout;

    check_mmdev();

    hr = DirectSoundCreate(NULL, &ds, NULL);
    if(FAILED(hr))
    {
        fprintf(f, "DSC failed: %08lx\n", hr);
        return 1;
    }

    hr = IDirectSound_SetCooperativeLevel(ds, GetDesktopWindow(), DSSCL_PRIORITY);
    if(FAILED(hr))
        fprintf(f, "SCL failed: %08lx\n", hr);

    if(!skip_primary){
        desc.dwSize = sizeof(desc);
        desc.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRL3D;
        desc.dwBufferBytes = 0;
        desc.dwReserved = 0;
        desc.lpwfxFormat = NULL;
        memset(&desc.guid3DAlgorithm, 0, sizeof(GUID));

        hr = IDirectSound_CreateSoundBuffer(ds, &desc, &primary, NULL);
        if(FAILED(hr))
            fprintf(f, "CSB(primary) failed: %08lx\n", hr);


        hr = IDirectSoundBuffer_GetFormat(primary, NULL, 0, &fmt_sz);
        if(FAILED(hr))
            fprintf(f, "GF(null) failed: %08lx\n", hr);

        fmtex = HeapAlloc(GetProcessHeap(), 0, fmt_sz);

        hr = IDirectSoundBuffer_GetFormat(primary, fmtex, fmt_sz, NULL);
        if(FAILED(hr))
            fprintf(f, "GF(alloced) failed: %08lx\n", hr);

        fprintf(f, "before secondary, primary has %u channels\n", fmtex->nChannels);

        hr = IDirectSoundBuffer_SetFormat(primary, fmtex);
        if(FAILED(hr))
            fprintf(f, "SF(primary) failed: %08lx\n", hr);

        HeapFree(GetProcessHeap(), 0, fmtex);


        hr = IDirectSoundBuffer_GetFormat(primary, NULL, 0, &fmt_sz);
        if(FAILED(hr))
            fprintf(f, "GF(null) failed: %08lx\n", hr);

        fmtex = HeapAlloc(GetProcessHeap(), 0, fmt_sz);

        hr = IDirectSoundBuffer_GetFormat(primary, fmtex, fmt_sz, NULL);
        if(FAILED(hr))
            fprintf(f, "GF(alloced) failed: %08lx\n", hr);

        fprintf(f, "after set, primary has %u channels\n", fmtex->nChannels);

        HeapFree(GetProcessHeap(), 0, fmtex);

    }

    sec_fmt.Format.wFormatTag = WAVE_FORMAT_EXTENSIBLE;
    sec_fmt.Format.nChannels = 6;
    sec_fmt.Format.nSamplesPerSec = 44100;
    sec_fmt.Format.wBitsPerSample = 16;
    sec_fmt.Format.nBlockAlign = sec_fmt.Format.nChannels * sec_fmt.Format.wBitsPerSample / 8;
    sec_fmt.Format.nAvgBytesPerSec = sec_fmt.Format.nSamplesPerSec * sec_fmt.Format.nBlockAlign;
    sec_fmt.Format.cbSize = sizeof(WAVEFORMATEXTENSIBLE) - sizeof(WAVEFORMATEX);
    sec_fmt.Samples.wValidBitsPerSample = 16;
    sec_fmt.dwChannelMask = KSAUDIO_SPEAKER_5POINT1;
    sec_fmt.SubFormat = pcm_subtype;

    desc.dwSize = sizeof(desc);
    desc.dwFlags = /*DSBCAPS_LOCHARDWARE | */DSBCAPS_CTRLFREQUENCY |
        DSBCAPS_CTRLPAN | DSBCAPS_CTRLVOLUME | DSBCAPS_GLOBALFOCUS |
        DSBCAPS_GETCURRENTPOSITION2;
    desc.dwBufferBytes = sizeof(six_chan_dat);
    desc.dwReserved = 0;
    desc.lpwfxFormat = (WAVEFORMATEX*)&sec_fmt;
    memset(&desc.guid3DAlgorithm, 0, sizeof(GUID));

    hr = IDirectSound_CreateSoundBuffer(ds, &desc, &secondary, NULL);
    if(FAILED(hr))
        fprintf(f, "CSB(2nd) failed: %08lx\n", hr);

    hr = IDirectSoundBuffer_Lock(secondary, 0, sizeof(six_chan_dat), &audio1, &audio1_len, &audio2, &audio2_len, 0);
    if(FAILED(hr))
        fprintf(f, "L failed: %08lx\n", hr);

    if(audio1 && audio1_len > 0)
        memcpy(audio1, six_chan_dat, min(audio1_len, sizeof(six_chan_dat)));
    if(audio2 && audio2_len > 0)
    {
        DWORD to_copy = 0;
        if(audio1_len < sizeof(six_chan_dat))
        {
            to_copy = min(audio2_len, sizeof(six_chan_dat) - audio1_len);
            memcpy(audio2, six_chan_dat + audio1_len, to_copy);
        }
        memset(audio2 + to_copy, 0, audio2_len - to_copy);
    }

    hr = IDirectSoundBuffer_Unlock(secondary, audio1, audio1_len, audio2, audio2_len);
    if(FAILED(hr))
        fprintf(f, "U failed: %08lx\n", hr);

    hr = IDirectSoundBuffer_Play(secondary, 0, 0, DSBPLAY_LOOPING);
    if(FAILED(hr))
        fprintf(f, "P failed: %08lx\n", hr);

    Sleep(1000);

    if(skip_primary)
    {
        desc.dwSize = sizeof(desc);
        desc.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRL3D;
        desc.dwBufferBytes = 0;
        desc.dwReserved = 0;
        desc.lpwfxFormat = NULL;
        memset(&desc.guid3DAlgorithm, 0, sizeof(GUID));

        hr = IDirectSound_CreateSoundBuffer(ds, &desc, &primary, NULL);
        if(FAILED(hr))
            fprintf(f, "CSB(prim) failed: %08lx\n", hr);
    }

    hr = IDirectSoundBuffer_GetFormat(primary, NULL, 0, &fmt_sz);
    if(FAILED(hr))
        fprintf(f, "GF(null) failed: %08lx\n", hr);

    fmtex = HeapAlloc(GetProcessHeap(), 0, fmt_sz);

    hr = IDirectSoundBuffer_GetFormat(primary, fmtex, fmt_sz, NULL);
    if(FAILED(hr))
        fprintf(f, "GF(alloced) failed: %08lx\n", hr);

    fprintf(f, "after secondary, primary has %u channels\n", fmtex->nChannels);

    if(skip_primary)
    {
        hr = IDirectSoundBuffer_SetFormat(primary, fmtex);
        if(FAILED(hr))
            fprintf(f, "SF(primary) failed: %08lx\n", hr);

        HeapFree(GetProcessHeap(), 0, fmtex);

        hr = IDirectSoundBuffer_GetFormat(primary, NULL, 0, &fmt_sz);
        if(FAILED(hr))
            fprintf(f, "GF(null) failed: %08lx\n", hr);

        fmtex = HeapAlloc(GetProcessHeap(), 0, fmt_sz);

        hr = IDirectSoundBuffer_GetFormat(primary, fmtex, fmt_sz, NULL);
        if(FAILED(hr))
            fprintf(f, "GF(alloced) failed: %08lx\n", hr);

        fprintf(f, "after set, primary has %u channels\n", fmtex->nChannels);
    }

    HeapFree(GetProcessHeap(), 0, fmtex);

    while(1){
        Sleep(100);
    }

    return 0;
}
