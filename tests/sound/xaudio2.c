#define COBJMACROS
#include "initguid.h"
#include "xaudio2.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>

static IXAudio27SourceVoice *g_src;
static HANDLE g_file;
static UINT32 g_accum_decode, g_nba;
static BYTE *g_audio_bytes;
static BOOL g_done = FALSE;

static void ok_(const char *file, int line, BOOL success, const char *fmt, ...) __attribute__((format(printf, 4, 5)));
#define ok(success, fmt, ...) ok_(__FILE__, __LINE__, success, fmt, ##__VA_ARGS__)
static void ok_(const char *file, int line, BOOL success, const char *fmt, ...)
{
    if(!success){
        va_list va;
        va_start(va, fmt);
        fprintf(stdout, "test failed (%s:%u): ", file, line);
        vfprintf(stdout, fmt, va);
        va_end(va);
    }
}

static void submit_buffer(void)
{
    XAUDIO2_BUFFER buf;
    XAUDIO2_BUFFER_WMA wmabuf;
    DWORD readed;
    HRESULT hr;
    BOOL br;
    UINT32 whatever;

    {
        static UINT64 prev_samples = 0;
        XAUDIO2_VOICE_STATE state;

        IXAudio27SourceVoice_GetState(g_src, &state);
        ok(0, "got samples played: %llu (delta: %llu)\n", state.SamplesPlayed, state.SamplesPlayed - prev_samples);

        prev_samples = state.SamplesPlayed;
    }

    g_audio_bytes = HeapAlloc(GetProcessHeap(), 0, g_nba);
    br = ReadFile(g_file, g_audio_bytes, g_nba, &readed, NULL);
    if(br != TRUE || readed != g_nba)
    {
        ok(0, "done reading file\n");
        g_done = TRUE;
        return;
    }

    memset(&buf, 0, sizeof(buf));
    memset(&wmabuf, 0, sizeof(wmabuf));

    buf.AudioBytes = g_nba;
    buf.pAudioData = g_audio_bytes;

    g_accum_decode += 600;
    whatever = g_accum_decode;
    wmabuf.PacketCount = 1;
    wmabuf.pDecodedPacketCumulativeBytes = &whatever;

    ok(0, "submitting\n");
    hr = IXAudio2SourceVoice_SubmitSourceBuffer(g_src, &buf, &wmabuf);
    ok(hr == S_OK, "SubmitSourceBuffer failed\n");
}

static IXAudio2VoiceCallback vcb1;

static void WINAPI VCB_OnVoiceProcessingPassStart(IXAudio2VoiceCallback *This,
        UINT32 BytesRequired)
{
}

static void WINAPI VCB_OnVoiceProcessingPassEnd(IXAudio2VoiceCallback *This)
{
}

static void WINAPI VCB_OnStreamEnd(IXAudio2VoiceCallback *This)
{
}

static void WINAPI VCB_OnBufferStart(IXAudio2VoiceCallback *This,
        void *pBufferContext)
{
}

static void WINAPI VCB_OnBufferEnd(IXAudio2VoiceCallback *This,
        void *pBufferContext)
{
    submit_buffer();
}

static void WINAPI VCB_OnLoopEnd(IXAudio2VoiceCallback *This,
        void *pBufferContext)
{
}

static void WINAPI VCB_OnVoiceError(IXAudio2VoiceCallback *This,
        void *pBuffercontext, HRESULT Error)
{
}

static IXAudio2VoiceCallbackVtbl vcb_vtbl = {
    VCB_OnVoiceProcessingPassStart,
    VCB_OnVoiceProcessingPassEnd,
    VCB_OnStreamEnd,
    VCB_OnBufferStart,
    VCB_OnBufferEnd,
    VCB_OnLoopEnd,
    VCB_OnVoiceError
};

static IXAudio2VoiceCallback vcb1 = { &vcb_vtbl };

static HRESULT prep_file(HANDLE *out_handle, WAVEFORMATEX **out_fmt)
{
    HANDLE h;
    struct {
        DWORD tag;
        DWORD size;
    } t;
    DWORD readed;
    BOOL br;
    WAVEFORMATEX *fmt;

    h = CreateFileA("test.wav", GENERIC_READ, 0, NULL, OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL, NULL);
    ok(h != INVALID_HANDLE_VALUE, "couldn't create file\n");
    if(h == INVALID_HANDLE_VALUE)
        return E_INVALIDARG;

    /* read RIFF tag */
    br = ReadFile(h, &t, sizeof(t), &readed, NULL);
    ok(br == TRUE && readed == sizeof(t), "failed to read RIFF tag\n");

    /* read XWMA */
    br = ReadFile(h, &t.tag, sizeof(t.tag), &readed, NULL);
    ok(br == TRUE && readed == sizeof(t.tag), "failed to read XWMA tag\n");

    /* read fmt  tag */
    br = ReadFile(h, &t, sizeof(t), &readed, NULL);
    ok(br == TRUE && readed == sizeof(t), "failed to read fmt tag\n");

    /* read format struct */
    fmt = HeapAlloc(GetProcessHeap(), 0, t.size);

    br = ReadFile(h, fmt, t.size, &readed, NULL);
    ok(br == TRUE && readed == t.size, "failed to read format\n");

    /* read data tag */
    br = ReadFile(h, &t, sizeof(t), &readed, NULL);
    ok(br == TRUE && readed == sizeof(t), "failed to read data tag\n");

    *out_handle = h;
    *out_fmt = fmt;

    return S_OK;
}

static void test_xwma(IXAudio27 *xa27)
{
    IXAudio27MasteringVoice *master;
    IXAudio27SourceVoice *src;
    WAVEFORMATEX *fmt;
    HANDLE file;
    HRESULT hr;
    UINT64 prev_samples = 0;

    hr = prep_file(&file, &fmt);
    ok(hr == S_OK, "something went wrong prepping file\n");
    if(FAILED(hr))
        return;

    hr = IXAudio27_CreateMasteringVoice(xa27, (IXAudio2MasteringVoice**)&master, 2, 44100, 0, 0, NULL);
    ok(hr == S_OK, "CreateMasteringVoice failed\n");

    hr = IXAudio27_CreateSourceVoice(xa27, (IXAudio2SourceVoice**)&src, fmt, 0, 1.f, &vcb1, NULL, NULL);
    ok(hr == S_OK, "CreateSourceVoice failed\n");

    g_file = file;
    g_audio_bytes = HeapAlloc(GetProcessHeap(), 0, fmt->nBlockAlign);
    g_nba = fmt->nBlockAlign * 16;
    g_src = src;
    g_accum_decode = 0;

    submit_buffer();
    submit_buffer();
    submit_buffer();
    submit_buffer();
    submit_buffer();
    submit_buffer();
    submit_buffer();
    submit_buffer();

    submit_buffer();
    submit_buffer();
    submit_buffer();
    submit_buffer();
    submit_buffer();
    submit_buffer();
    submit_buffer();
    submit_buffer();

    hr = IXAudio2SourceVoice_Start(src, 0, XAUDIO2_COMMIT_NOW);
    ok(hr == S_OK, "Start failed\n");

    hr = IXAudio2_StartEngine(xa27);
    ok(hr == S_OK, "StartEngine failed\n");

    while(!g_done)
    {
        XAUDIO2_VOICE_STATE state;

        IXAudio27SourceVoice_GetState(src, &state);
        //ok(0, "got samples played: %llu (delta: %llu)\n", state.SamplesPlayed, state.SamplesPlayed - prev_samples);

        prev_samples = state.SamplesPlayed;

        Sleep(100);
    }

    IXAudio2_StopEngine(xa27);

    IXAudio27SourceVoice_DestroyVoice(src);
    IXAudio27MasteringVoice_DestroyVoice(master);
}

int main(int argc, char **argv)
{
    HRESULT hr;
    IXAudio27 *xa27;

    CoInitialize(NULL);

    hr = CoCreateInstance(&CLSID_XAudio27, NULL, CLSCTX_INPROC_SERVER,
            &IID_IXAudio27, (void**)&xa27);
    ok(hr == S_OK, "Failed to create XA27 object\n");
    if (hr != S_OK)
        return 1;

    hr = IXAudio27_Initialize(xa27, 0, XAUDIO2_ANY_PROCESSOR);
    ok(hr == S_OK, "Initialize failed: %08lx\n", hr);

    test_xwma(xa27);

    IXAudio27_Release(xa27);

    return 0;
}
