#include <windows.h>
#include <stdio.h>

static FILE *f;
static BOOL quit;

static LRESULT WINAPI our_wnd_proc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    switch(msg)
    {
        case WM_LBUTTONDOWN:
        case WM_RBUTTONDOWN:
        case WM_XBUTTONDOWN:
            {
                fprintf(f, "btn msg %x: w: %x l: %x\n",
                        msg, wparam, lparam);
            }
            break;
        case WM_DESTROY:
            quit = TRUE;
            break;
    }
    return DefWindowProcA(hwnd, msg, wparam, lparam);
}

static void test_my(void)
{
    WNDCLASSA cls;
    MSG msg;
    HWND hwnd;

    quit = FALSE;

    memset(&cls, 0, sizeof(cls));
    cls.lpfnWndProc = our_wnd_proc;
    cls.hInstance = GetModuleHandleA(NULL);
    cls.hCursor = LoadCursorA(0, (LPCSTR)IDC_ARROW);
    cls.hbrBackground = GetStockObject(WHITE_BRUSH);
    cls.lpszClassName = "our_class";
    RegisterClassA(&cls);

    hwnd = CreateWindowExA(0, "our_class", "window",
            WS_CAPTION | WS_SYSMENU | WS_VISIBLE, 0, 0, 100, 100,
            GetDesktopWindow(), NULL, GetModuleHandleA(NULL), NULL);
    ShowWindow(hwnd, SW_SHOW);

    while(!quit){
        while(PeekMessageA(&msg, hwnd, 0, 0, PM_REMOVE) != 0)
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    DestroyWindow(hwnd);
}

int main(int argc, char **argv)
{
    if(argc > 1)
        f = fopen(argv[1], "w");
    else
        f = stdout;

    test_my();
    fclose(f);
    return 0;
}
