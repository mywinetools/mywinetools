
#define HID_CTL_CODE(id) \
  CTL_CODE (FILE_DEVICE_KEYBOARD, (id), METHOD_NEITHER, FILE_ANY_ACCESS)

#define IOCTL_HID_GET_STRING                     HID_CTL_CODE(4)



#include <stdio.h>
#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windows.h"
#include "setupapi.h"
#include "hidusage.h"
#include "winioctl.h"
#include "ddk/hidsdi.h"
#include "ddk/hidclass.h"
//#include "ddk/hidport.h"
#include "xinput.h"
#include <stdarg.h>
#include <stdint.h>

//#include "wine/test.h"

//#define READ_MAX_TIME 5000

static FILE *f;

#if 0
static void test_my(void)
{
    RAWINPUTDEVICELIST *list;
    UINT ur, ndevs = 0, devi, i, sz;
    BOOL br;
    char path[2048];
    PHIDP_PREPARSED_DATA parsed;
    HIDP_CAPS caps;
    HIDP_BUTTON_CAPS *button_caps;
    HIDP_VALUE_CAPS *value_caps;
    HIDP_DATA *data_list;
    USHORT nbutton_caps;
    USHORT nvalue_caps;
    USHORT ntries;
    ULONG ndata_list;
    HANDLE dev;
    NTSTATUS nts;
    DWORD junk;
    WCHAR string[2048];
    char stringA[2048];

    ur = GetRawInputDeviceList(NULL, &ndevs, sizeof(*list));
    if(!(ur != -1)) fprintf(f,  "GRIDL failed\n");

    if(!(0)) fprintf(f,  "got %u devs\n", ndevs);

    list = HeapAlloc(GetProcessHeap(), 0, ndevs);

    ur = GetRawInputDeviceList(list, &ndevs, sizeof(*list));
    if(!(ur != -1)) fprintf(f,  "GRIDL failed\n");

    for(devi = 0; devi < ndevs; ++devi){
        if(list[devi].dwType == RIM_TYPEHID){
            sz = sizeof(path) / sizeof(*path);
            ur = GetRawInputDeviceInfoA(list[devi].hDevice, RIDI_DEVICENAME,
                    path, &sz);
            if(!(ur != -1)) fprintf(f,  "GRIDI failed\n");

            if(!(0)) fprintf(f,  "got hid device path: %s\n",path);
            if(strstr(path, "WINEMOUSE"))
                continue;

            dev = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
                    NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
            if(!(dev != INVALID_HANDLE_VALUE))
            {
                fprintf(f,  "CreateFile failed\n");
                return;
            }

            memset(string, 0, sizeof(string));
            DeviceIoControl(dev, 0xb01be, NULL, 0, string, sizeof(string), &junk, NULL);
            WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
            fprintf(f, "got device string: %s\n", stringA);

            HIDD_ATTRIBUTES attr;
            HidD_GetAttributes(dev, &attr);
            fprintf(f, "got vid 0x%hx, pid 0x%hx, ver 0x%hx\n", attr.VendorID, attr.ProductID, attr.VersionNumber);

            memset(string, 0, sizeof(string));
            if(HidD_GetManufacturerString(dev, string, sizeof(string)))
            {
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got manufacturer string: %s\n", stringA);
            }else
                fprintf(f, "GetManufacturerString failed: 0x%x\n", GetLastError());

            memset(string, 0, sizeof(string));
            if(HidD_GetProductString(dev, string, sizeof(string)))
            {
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got product string: %s\n", stringA);
            }else
                fprintf(f, "GetProductString failed: 0x%x\n", GetLastError());

            memset(string, 0, sizeof(string));
            if(HidD_GetSerialNumberString(dev, string, sizeof(string))){
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got serial number string: %s\n", stringA);
            }else
                fprintf(f, "GetSerialNumberString failed: 0x%x\n", GetLastError());

            br = HidD_GetPreparsedData(dev, &parsed);
            fprintf(f, "a\n");
            if(!(br == TRUE))
            {
                fprintf(f,  "GetPreparsedData failed\n");
                CloseHandle(dev);
                return;
            }

            fprintf(f, "b\n");
            br = HidP_GetCaps(parsed, &caps);
            fprintf(f, "c\n");
            if(!(br == HIDP_STATUS_SUCCESS)) fprintf(f,  "GetCaps failed\n");

            fprintf(f, "Usage: %u\n", caps.Usage);
            fprintf(f, "UsagePage: %u\n", caps.UsagePage);
            fprintf(f, "InputReportByteLength: %hu\n", caps.InputReportByteLength);
            fprintf(f, "OutputReportByteLength: %hu\n", caps.OutputReportByteLength);
            fprintf(f, "FeatureReportByteLength: %hu\n", caps.FeatureReportByteLength);
            //fprintf(f, "Reserved: %hu\n", caps.Reserved);
            fprintf(f, "NumberLinkCollectionNodes: %hu\n", caps.NumberLinkCollectionNodes);
            fprintf(f, "NumberInputButtonCaps: %hu\n", caps.NumberInputButtonCaps);
            fprintf(f, "NumberInputValueCaps: %hu\n", caps.NumberInputValueCaps);
            fprintf(f, "NumberInputDataIndices: %hu\n", caps.NumberInputDataIndices);
            fprintf(f, "NumberOutputButtonCaps: %hu\n", caps.NumberOutputButtonCaps);
            fprintf(f, "NumberOutputValueCaps: %hu\n", caps.NumberOutputValueCaps);
            fprintf(f, "NumberOutputDataIndices: %hu\n", caps.NumberOutputDataIndices);
            fprintf(f, "NumberFeatureButtonCaps: %hu\n", caps.NumberFeatureButtonCaps);
            fprintf(f, "NumberFeatureValueCaps: %hu\n", caps.NumberFeatureValueCaps);
            fprintf(f, "NumberFeatureDataIndices: %hu\n", caps.NumberFeatureDataIndices);


            nbutton_caps = caps.NumberInputButtonCaps;
            button_caps = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, nbutton_caps * sizeof(*button_caps));

            br = HidP_GetButtonCaps(HidP_Input, button_caps, &nbutton_caps, parsed);
            if(!(br == HIDP_STATUS_SUCCESS)) fprintf(f,  "GetButtonCaps failed\n");

            for(i = 0; i < nbutton_caps; ++i){
                fprintf(f, "%u, UsagePage: %hu\n", i, button_caps[i].UsagePage);
                fprintf(f, "%u, ReportID: %hhu\n", i, button_caps[i].ReportID);
                fprintf(f, "%u, IsAlias: %hhu\n", i, button_caps[i].IsAlias);
                fprintf(f, "%u, BitField: %hu\n", i, button_caps[i].BitField);
                fprintf(f, "%u, LinkCollection: %hu\n", i, button_caps[i].LinkCollection);
                fprintf(f, "%u, LinkUsage: %hu\n", i, button_caps[i].LinkUsage);
                fprintf(f, "%u, LinkUsagePage: %hu\n", i, button_caps[i].LinkUsagePage);
                fprintf(f, "%u, IsRange: %hhu\n", i, button_caps[i].IsRange);
                fprintf(f, "%u, IsStringRange: %hhu\n", i, button_caps[i].IsStringRange);
                fprintf(f, "%u, IsDesignatorRange: %hhu\n", i, button_caps[i].IsDesignatorRange);
                fprintf(f, "%u, IsAbsolute: %hhu\n", i, button_caps[i].IsAbsolute);
                    //ULONG    Reserved[10];
                if(button_caps[i].IsRange){
                    fprintf(f, "%u, UsageMin: %hu\n", i, button_caps[i].Range.UsageMin);
                    fprintf(f, "%u, UsageMax: %hu\n", i, button_caps[i].Range.UsageMax);
                    fprintf(f, "%u, StringMin: %hu\n", i, button_caps[i].Range.StringMin);
                    fprintf(f, "%u, StringMax: %hu\n", i, button_caps[i].Range.StringMax);
                    fprintf(f, "%u, DesignatorMin: %hu\n", i, button_caps[i].Range.DesignatorMin);
                    fprintf(f, "%u, DesignatorMax: %hu\n", i, button_caps[i].Range.DesignatorMax);
                    fprintf(f, "%u, DataIndexMin: %hu\n", i, button_caps[i].Range.DataIndexMin);
                    fprintf(f, "%u, DataIndexMax: %hu\n", i, button_caps[i].Range.DataIndexMax);
                }else{
                    fprintf(f, "%u, Usage: %hu\n", i, button_caps[i].NotRange.Usage);
                    fprintf(f, "%u, Reserved1: %hu\n", i, button_caps[i].NotRange.Reserved1);
                    fprintf(f, "%u, StringIndex: %hu\n", i, button_caps[i].NotRange.StringIndex);
                    fprintf(f, "%u, Reserved2: %hu\n", i, button_caps[i].NotRange.Reserved2);
                    fprintf(f, "%u, DesignatorIndex: %hu\n", i, button_caps[i].NotRange.DesignatorIndex);
                    fprintf(f, "%u, Reserved3: %hu\n", i, button_caps[i].NotRange.Reserved3);
                    fprintf(f, "%u, DataIndex: %hu\n", i, button_caps[i].NotRange.DataIndex);
                    fprintf(f, "%u, Reserved4: %hu\n", i, button_caps[i].NotRange.Reserved4);
                }
                fprintf(f, "\n");
            }

            nvalue_caps = caps.NumberInputValueCaps;
            value_caps = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, nvalue_caps * sizeof(*value_caps));
            br = HidP_GetValueCaps(HidP_Input, value_caps, &nvalue_caps, parsed);
            if(!(br == HIDP_STATUS_SUCCESS)) fprintf(f,  "GetValueCaps failed\n");

            for(i = 0; i < nvalue_caps; ++i){
                fprintf(f, "%u, UsagePage: %hu\n", i, value_caps[i].UsagePage);
                fprintf(f, "%u, ReportID: %hhu\n", i, value_caps[i].ReportID);
                fprintf(f, "%u, IsAlias: %hhu\n", i, value_caps[i].IsAlias);
                fprintf(f, "%u, BitField: %hu\n", i, value_caps[i].BitField);
                fprintf(f, "%u, LinkCollection: %hu\n", i, value_caps[i].LinkCollection);
                fprintf(f, "%u, LinkUsage: %hu\n", i, value_caps[i].LinkUsage);
                fprintf(f, "%u, LinkUsagePage: %hu\n", i, value_caps[i].LinkUsagePage);
                fprintf(f, "%u, IsRange: %hhu\n", i, value_caps[i].IsRange);
                fprintf(f, "%u, IsStringRange: %hhu\n", i, value_caps[i].IsStringRange);
                fprintf(f, "%u, IsDesignatorRange: %hhu\n", i, value_caps[i].IsDesignatorRange);
                fprintf(f, "%u, IsAbsolute: %hhu\n", i, value_caps[i].IsAbsolute);
                fprintf(f, "%u, HasNull: %hhu\n", i, value_caps[i].HasNull);
                fprintf(f, "%u, Reserved: %hhu\n", i, value_caps[i].Reserved);
                fprintf(f, "%u, BitSize: %hu\n", i, value_caps[i].BitSize);
                fprintf(f, "%u, ReportCount: %hu\n", i, value_caps[i].ReportCount);
                //fprintf(f, "%u, Reserved2: %hu\n", i, value_caps[i].Reserved2)[5];
                fprintf(f, "%u, UnitsExp: %u\n", i, value_caps[i].UnitsExp);
                fprintf(f, "%u, Units: %u\n", i, value_caps[i].Units);
                fprintf(f, "%u, LogicalMin: %u\n", i, value_caps[i].LogicalMin);
                fprintf(f, "%u, LogicalMax: %u\n", i, value_caps[i].LogicalMax);
                fprintf(f, "%u, PhysicalMin: %u\n", i, value_caps[i].PhysicalMin);
                fprintf(f, "%u, PhysicalMax: %u\n", i, value_caps[i].PhysicalMax);

                if(value_caps[i].IsRange){
                    fprintf(f, "%u, UsageMin: %hu\n", i, value_caps[i].Range.UsageMin);
                    fprintf(f, "%u, UsageMax: %hu\n", i, value_caps[i].Range.UsageMax);
                    fprintf(f, "%u, StringMin: %hu\n", i, value_caps[i].Range.StringMin);
                    fprintf(f, "%u, StringMax: %hu\n", i, value_caps[i].Range.StringMax);
                    fprintf(f, "%u, DesignatorMin: %hu\n", i, value_caps[i].Range.DesignatorMin);
                    fprintf(f, "%u, DesignatorMax: %hu\n", i, value_caps[i].Range.DesignatorMax);
                    fprintf(f, "%u, DataIndexMin: %hu\n", i, value_caps[i].Range.DataIndexMin);
                    fprintf(f, "%u, DataIndexMax: %hu\n", i, value_caps[i].Range.DataIndexMax);
                }else{
                    fprintf(f, "%u, Usage: %hu\n", i, value_caps[i].NotRange.Usage);
                    fprintf(f, "%u, Reserved1: %hu\n", i, value_caps[i].NotRange.Reserved1);
                    fprintf(f, "%u, StringIndex: %hu\n", i, value_caps[i].NotRange.StringIndex);
                    fprintf(f, "%u, Reserved2: %hu\n", i, value_caps[i].NotRange.Reserved2);
                    fprintf(f, "%u, DesignatorIndex: %hu\n", i, value_caps[i].NotRange.DesignatorIndex);
                    fprintf(f, "%u, Reserved3: %hu\n", i, value_caps[i].NotRange.Reserved3);
                    fprintf(f, "%u, DataIndex: %hu\n", i, value_caps[i].NotRange.DataIndex);
                    fprintf(f, "%u, Reserved4: %hu\n", i, value_caps[i].NotRange.Reserved4);
                }
                fprintf(f, "\n");
            }

            nvalue_caps = caps.NumberFeatureValueCaps;
            value_caps = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, nvalue_caps * sizeof(*value_caps));
            br = HidP_GetValueCaps(HidP_Feature, value_caps, &nvalue_caps, parsed);
            if(!(br == HIDP_STATUS_SUCCESS)) fprintf(f,  "GetValueCaps failed\n");

            for(i = 0; i < nvalue_caps; ++i){
                fprintf(f, "%u, UsagePage: %hu\n", i, value_caps[i].UsagePage);
                fprintf(f, "%u, ReportID: %hhu\n", i, value_caps[i].ReportID);
                fprintf(f, "%u, IsAlias: %hhu\n", i, value_caps[i].IsAlias);
                fprintf(f, "%u, BitField: %hu\n", i, value_caps[i].BitField);
                fprintf(f, "%u, LinkCollection: %hu\n", i, value_caps[i].LinkCollection);
                fprintf(f, "%u, LinkUsage: %hu\n", i, value_caps[i].LinkUsage);
                fprintf(f, "%u, LinkUsagePage: %hu\n", i, value_caps[i].LinkUsagePage);
                fprintf(f, "%u, IsRange: %hhu\n", i, value_caps[i].IsRange);
                fprintf(f, "%u, IsStringRange: %hhu\n", i, value_caps[i].IsStringRange);
                fprintf(f, "%u, IsDesignatorRange: %hhu\n", i, value_caps[i].IsDesignatorRange);
                fprintf(f, "%u, IsAbsolute: %hhu\n", i, value_caps[i].IsAbsolute);
                fprintf(f, "%u, HasNull: %hhu\n", i, value_caps[i].HasNull);
                fprintf(f, "%u, Reserved: %hhu\n", i, value_caps[i].Reserved);
                fprintf(f, "%u, BitSize: %hu\n", i, value_caps[i].BitSize);
                fprintf(f, "%u, ReportCount: %hu\n", i, value_caps[i].ReportCount);
                //fprintf(f, "%u, Reserved2: %hu\n", i, value_caps[i].Reserved2)[5];
                fprintf(f, "%u, UnitsExp: %u\n", i, value_caps[i].UnitsExp);
                fprintf(f, "%u, Units: %u\n", i, value_caps[i].Units);
                fprintf(f, "%u, LogicalMin: %u\n", i, value_caps[i].LogicalMin);
                fprintf(f, "%u, LogicalMax: %u\n", i, value_caps[i].LogicalMax);
                fprintf(f, "%u, PhysicalMin: %u\n", i, value_caps[i].PhysicalMin);
                fprintf(f, "%u, PhysicalMax: %u\n", i, value_caps[i].PhysicalMax);

                if(value_caps[i].IsRange){
                    fprintf(f, "%u, UsageMin: %hu\n", i, value_caps[i].Range.UsageMin);
                    fprintf(f, "%u, UsageMax: %hu\n", i, value_caps[i].Range.UsageMax);
                    fprintf(f, "%u, StringMin: %hu\n", i, value_caps[i].Range.StringMin);
                    fprintf(f, "%u, StringMax: %hu\n", i, value_caps[i].Range.StringMax);
                    fprintf(f, "%u, DesignatorMin: %hu\n", i, value_caps[i].Range.DesignatorMin);
                    fprintf(f, "%u, DesignatorMax: %hu\n", i, value_caps[i].Range.DesignatorMax);
                    fprintf(f, "%u, DataIndexMin: %hu\n", i, value_caps[i].Range.DataIndexMin);
                    fprintf(f, "%u, DataIndexMax: %hu\n", i, value_caps[i].Range.DataIndexMax);
                }else{
                    fprintf(f, "%u, Usage: %hu\n", i, value_caps[i].NotRange.Usage);
                    fprintf(f, "%u, Reserved1: %hu\n", i, value_caps[i].NotRange.Reserved1);
                    fprintf(f, "%u, StringIndex: %hu\n", i, value_caps[i].NotRange.StringIndex);
                    fprintf(f, "%u, Reserved2: %hu\n", i, value_caps[i].NotRange.Reserved2);
                    fprintf(f, "%u, DesignatorIndex: %hu\n", i, value_caps[i].NotRange.DesignatorIndex);
                    fprintf(f, "%u, Reserved3: %hu\n", i, value_caps[i].NotRange.Reserved3);
                    fprintf(f, "%u, DataIndex: %hu\n", i, value_caps[i].NotRange.DataIndex);
                    fprintf(f, "%u, Reserved4: %hu\n", i, value_caps[i].NotRange.Reserved4);
                }
                fprintf(f, "\n");
            }





            ndata_list = HidP_MaxDataListLength(HidP_Input, parsed);
            if(!(0)) fprintf(f,  "got max data length: 0x%x\n", ndata_list);

#if 0
            data_list = HeapAlloc(GetProcessHeap(), 0, ndata_list * sizeof(HIDP_DATA));

            while(1){
                DWORD tx;
                OVERLAPPED overlapped;
                unsigned char dat[64];
                ULONG len;
                DWORD junk;

                memset(&overlapped, 0, sizeof(overlapped));
                memset(dat, 0, sizeof(dat));

                br = ReadFile(dev, dat, sizeof(dat), &junk, NULL);
                if(!(br == TRUE)) fprintf(f,  "ReadFile should have given TRUE\n");
                fprintf(f, "got %u read\n", junk);

                for(i = 0; i < junk; ++i)
                {
                    fprintf(f, "%02hhx ", dat[i]);
                }
                fprintf(f, "\n");
                /*
                fprintf(f, "%02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx\n",
                        dat[0],
                        dat[1],
                        dat[2],
                        dat[3],
                        dat[4],
                        dat[5],
                        dat[6],
                        dat[7],
                        dat[8],
                        dat[9],
                        dat[10],
                        dat[11],
                        dat[12],
                        dat[13],
                        dat[14]);
                        */
                fflush(stdout);

#if 0
                while(br == FALSE){
        //            if(!(0)) fprintf(f,  "sleeping\n");
                    Sleep(100);
                    br = GetOverlappedResult(dev, &overlapped, &tx, FALSE);
                }
#endif

        //        if(!(0)) fprintf(f,  "--------------------\n");
                if(br){
                    len = ndata_list;
                    memset(data_list, 0, sizeof(*data_list) * ndata_list);
                    nts = HidP_GetData(HidP_Input, data_list, &len, parsed, dat, junk);
                    if(!(nts == HIDP_STATUS_SUCCESS)) fprintf(f,  "GetData failed\n");

                    for(i = 0; i < ndata_list; ++i){
                        fprintf(f,  "got data[%u]: %u --> 0x%x\n", i, data_list[i].DataIndex, data_list[i].RawValue);
                    }
                    fflush(stdout);
                }
                Sleep(10);
            }
#endif

            HidD_FreePreparsedData(parsed);

            CloseHandle(dev);
        }
    }
}
#endif
typedef struct
{
    uint8_t ucLeftJoystickX;
    uint8_t ucLeftJoystickY;
    uint8_t ucRightJoystickX;
    uint8_t ucRightJoystickY;
    uint8_t rgucButtonsHatAndCounter[ 3 ];
    uint8_t ucTriggerLeft;
    uint8_t ucTriggerRight;
    uint8_t _rgucPad0[ 3 ];
    int16_t sGyroX;
    int16_t sGyroY;
    int16_t sGyroZ;
    int16_t sAccelX;
    int16_t sAccelY;
    int16_t sAccelZ;
    uint8_t _rgucPad1[ 5 ];
    uint8_t ucBatteryLevel;
    uint8_t _rgucPad2[ 4 ];
    uint8_t ucTrackpadCounter1;
    uint8_t rgucTrackpadData1[ 3 ];
    uint8_t ucTrackpadCounter2;
    uint8_t rgucTrackpadData2[ 3 ];
} PS4StatePacket_t;

static void process_wm_input(LPARAM handle)
{
    UINT sz, i, j, ret;
    BOOLEAN br;
    RAWINPUT *info;
    PS4StatePacket_t *pkt;
    char *devname;
    unsigned char *ppd, *x;
    PHIDP_PREPARSED_DATA pppd;
    HANDLE dev;
    char trash[1024];

    fprintf(f, "--------\n");

    sz = 0;
    ret = GetRawInputData((HRAWINPUT)handle, RID_INPUT, NULL, &sz, sizeof(RAWINPUTHEADER));
    fprintf(f, "NULL data gave 0x%x\n", ret);

    info = HeapAlloc(GetProcessHeap(), 0, sz);

    ret = GetRawInputData((HRAWINPUT)handle, RID_INPUT, info, &sz, sizeof(RAWINPUTHEADER));
    fprintf(f, "real data gave 0x%x\n", ret);

    fprintf(f, "header.dwType: 0x%x\n", info->header.dwType);
    fprintf(f, "header.dwSize: 0x%x\n", info->header.dwSize);
    fprintf(f, "header.hDevice: 0x%x\n", info->header.hDevice);
    fprintf(f, "header.wParam: 0x%x\n", info->header.wParam);

    if(info->header.dwType == RIM_TYPEHID)
    {
        fprintf(f, "data.hid.dwSizeHid: 0x%x\n", info->data.hid.dwSizeHid);
        fprintf(f, "data.hid.dwCount: 0x%x\n", info->data.hid.dwCount);
    }

    sz = 0;
    ret = GetRawInputDeviceInfoA(info->header.hDevice, RIDI_DEVICENAME, NULL, &sz);
    fprintf(f, "NULL name gave 0x%x\n", ret);

    devname = HeapAlloc(GetProcessHeap(), 0, sz);

    ret = GetRawInputDeviceInfoA(info->header.hDevice, RIDI_DEVICENAME, devname, &sz);
    fprintf(f, "real name gave 0x%x\n", ret);

    fprintf(f, "got name: %s\n", devname);

    HeapFree(GetProcessHeap(), 0, devname);

    sz = sizeof(trash);
    ret = GetRawInputData((HRAWINPUT)handle, RID_INPUT, trash, &sz, sizeof(RAWINPUTHEADER));
    fprintf(f, "trash data gave 0x%x, in sz: 0x%x\n", ret, sz);

#if 0
    sz = 0;
    GetRawInputDeviceInfoA(info->header.hDevice, RIDI_PREPARSEDDATA, NULL, &sz);

    fprintf(f, "ppd is %u bytes\n", sz);

    ppd = HeapAlloc(GetProcessHeap(), 0, sz);

    GetRawInputDeviceInfoA(info->header.hDevice, RIDI_PREPARSEDDATA, ppd, &sz);

    {
        char path[MAX_PATH];
        UINT ur;
        sz = sizeof(path) / sizeof(*path);
        ur = GetRawInputDeviceInfoA(info->header.hDevice, RIDI_DEVICENAME,
                path, &sz);
        if(!(ur != -1)) fprintf(f,  "GRIDI failed\n");

        fprintf(f,  "got hid device path: %s\n",path);

        dev = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
                NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if(!(dev != INVALID_HANDLE_VALUE))
        {
            fprintf(f,  "CreateFile failed\n");
            return;
        }

        br = HidD_GetPreparsedData(dev, &pppd);
        if(!br)
            fprintf(f, "GetPPD failed\n");
    }

    fprintf(f, "from user32 %02hhx %02hhx %02hhx %02hhx\n",
            ppd[0],
            ppd[1],
            ppd[2],
            ppd[3]);

    x = (unsigned char *)pppd;
    fprintf(f, "from hid %02hhx %02hhx %02hhx %02hhx\n",
            x[0],
            x[1],
            x[2],
            x[3]);

    return;
#endif

//    fprintf(f, "got %ux%u bytes\n", info->data.hid.dwCount, info->data.hid.dwSizeHid);
    for(i = 0; i < info->data.hid.dwCount; ++i)
    {
        pkt = &info->data.hid.bRawData[info->data.hid.dwSizeHid * i + 1];
        fprintf(f, "lx: %02hhx ly: %02hhx rx: %02hhx ry: %02hhx, btn[0]: %02hhx, btn[1]: %02hhx, btn[2]: %02hhx\n",
                pkt->ucLeftJoystickX, pkt->ucLeftJoystickY,
                pkt->ucRightJoystickX, pkt->ucRightJoystickY,
                pkt->rgucButtonsHatAndCounter[0],
                pkt->rgucButtonsHatAndCounter[1],
                pkt->rgucButtonsHatAndCounter[2] & 0x3
                );
#if 0
        for(j = 0; j < info->data.hid.dwSizeHid; ++j)
        {
            if(j && j % 16 == 0)
                fprintf(f, "\n");
            else if(j && j % 2 == 0)
                fprintf(f, " ");
            fprintf(f, "%02x", (unsigned char)info->data.hid.bRawData[info->data.hid.dwSizeHid * i + j]);
        }
        fprintf(f, "\n----");
#endif
    }

#if 0
    HidD_FreePreparsedData(pppd);
    HeapFree(GetProcessHeap(), 0, ppd);
    HeapFree(GetProcessHeap(), 0, info);

    CloseHandle(dev);
#endif
}

static LRESULT WINAPI our_wnd_proc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    switch(msg)
    {
        case WM_DEVICECHANGE:
        {
            fprintf(f, "got device change\n");
            break;
        }
        case WM_INPUT:
        {
        //    fprintf(f, "got WM_INPUT\n");
            process_wm_input(lparam);
            break;
        }
    }
    return DefWindowProcA(hwnd, msg, wparam, lparam);
}

static void test_my(void)
{
    RAWINPUTDEVICELIST *list;
    RAWINPUTDEVICE rid[2];
    UINT ur, ndevs = 0, devi, i, sz;
    BOOL br;
    char path[2048];
    PHIDP_PREPARSED_DATA parsed;
    HIDP_CAPS caps;
    HIDP_BUTTON_CAPS *button_caps;
    HIDP_VALUE_CAPS *value_caps;
    HIDP_DATA *data_list;
    USHORT nbutton_caps;
    USHORT nvalue_caps;
    USHORT ntries;
    ULONG ndata_list;
    HANDLE dev;
    NTSTATUS nts;
    DWORD junk;
    WCHAR string[2048];
    char stringA[2048];
    WNDCLASSA cls;
    HWND hwnd;
    MSG msg;

    memset(&cls, 0, sizeof(cls));
    cls.lpfnWndProc = our_wnd_proc;
    cls.hInstance = GetModuleHandleA(NULL);
    cls.hCursor = LoadCursorA(0, (LPCSTR)IDC_ARROW);
    cls.hbrBackground = GetStockObject(WHITE_BRUSH);
    cls.lpszClassName = "our_class";
    RegisterClassA(&cls);

    hwnd = CreateWindowExA(0, "our_class", "window",
            WS_CAPTION | WS_SYSMENU | WS_VISIBLE, 0, 0, 100, 100,
            GetDesktopWindow(), NULL, GetModuleHandleA(NULL), NULL);
    ShowWindow(hwnd, SW_SHOW);

#if 0
    ur = GetRawInputDeviceList(NULL, &ndevs, sizeof(*list));
    if(!(ur != -1)) fprintf(f,  "GRIDL failed\n");

    list = HeapAlloc(GetProcessHeap(), 0, ndevs);

    ur = GetRawInputDeviceList(list, &ndevs, sizeof(*list));
    if(!(ur != -1)) fprintf(f,  "GRIDL failed\n");

    for(devi = 0; devi < ndevs; ++devi){
        if(list[devi].dwType == RIM_TYPEHID){
            sz = sizeof(path) / sizeof(*path);
            ur = GetRawInputDeviceInfoA(list[devi].hDevice, RIDI_DEVICENAME,
                    path, &sz);
            if(!(ur != -1)) fprintf(f,  "GRIDI failed\n");

            fprintf(f,  "got hid device path: %s\n",path);
            if(strstr(path, "WINEMOUSE"))
                continue;

            memset(&rid, 0, sizeof(rid));
            rid.usUsagePage = 0x1;
            rid.usUsage = 0x4;
   //         rid.dwFlags = RIDEV_INPUTSINK;
            rid.hwndTarget = hwnd;

            RegisterRawInputDevices(&rid, 1, sizeof(rid));

#if 0
            dev = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
                    NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
            if(!(dev != INVALID_HANDLE_VALUE))
            {
                fprintf(f,  "CreateFile failed\n");
                return;
            }
#endif
        }
    }
#endif
    rid[0].usUsagePage = 0x1;
    rid[0].usUsage = 0x4;
    rid[0].dwFlags = 0;
    rid[0].hwndTarget = 0;
    rid[1].usUsagePage = 0x1;
    rid[1].usUsage = 0x5;
    rid[1].dwFlags = 0;
    rid[1].hwndTarget = 0;
    RegisterRawInputDevices(rid, 2, sizeof(*rid));

    do{
        while(PeekMessageA(&msg, hwnd, 0, 0, PM_REMOVE) != 0)
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }while(1);

}

int main(int argc, char **argv)
{
    if(argc > 1)
        f = fopen(argv[1], "w");
    else
        f = stdout;

    test_my();
    fclose(f);
    return 0;
}
