#include <SDL2/SDL.h>
#include <stdio.h>

FILE *f;

int main(int argc, char **argv)
{
    SDL_Joystick *device;
    int i, j;

    if(argc > 1)
        f = fopen(argv[1], "w");
    else
        f = stderr;

    SDL_Init(SDL_INIT_JOYSTICK);
    SDL_JoystickEventState(SDL_ENABLE);

    for(i = 0; i < SDL_NumJoysticks(); ++i){
        device = SDL_JoystickOpen(i);
        fprintf(f, "got device %u %p: %s\n", i, device, SDL_JoystickName(device));
        while(1){
            SDL_JoystickUpdate();
            fprintf(f, "with %u buttons:", SDL_JoystickNumButtons(device));
            for(j = 0; j < SDL_JoystickNumButtons(device); ++j){
                if(SDL_JoystickGetButton(device, j))
                    fprintf(f, " %u", j);
            }
#if 0
            for(j = 0; j < SDL_JoystickNumAxes(device); ++j){
                fprintf(f, " %08x", SDL_JoystickGetAxis(device, j));
            }
#endif
            fprintf(f, "\n");
            usleep(100000);
        }
    }

    fprintf(f, "Done.\n");

    return 0;
}
