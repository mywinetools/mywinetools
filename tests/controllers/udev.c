#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <libudev.h>
#include <linux/hidraw.h>

static FILE *f;

static void dump_list(struct udev_list_entry *head)
{
    struct udev_list_entry *entry;

    udev_list_entry_foreach(entry, head)
    {
        const char *name = udev_list_entry_get_name(entry);
        const char *value = udev_list_entry_get_value(entry);

        fprintf(f, "%s --> %s\n", name, value);
    }
    fprintf(f, "\n");
}

static void dump_dev_tree(struct udev_device *dev)
{
    if(!dev)
        return;

    fprintf(f, "----------------------------------------------\n");
    fprintf(f, "here is %s:\n\n", udev_device_get_syspath(dev));

    fprintf(f, "dumping sysattr list:\n");
    dump_list(udev_device_get_sysattr_list_entry(dev));

    fprintf(f, "dumping properties list:\n");
    dump_list(udev_device_get_properties_list_entry(dev));

    dump_dev_tree(udev_device_get_parent(dev));
}

int main(int argc, char **argv)
{
    struct udev_enumerate *enm;
    struct udev *ctx;
    struct udev_list_entry *devs, *dev_entry;
    struct udev_device *dev, *hiddev;

    if(argc > 1)
        f = fopen(argv[1], "w");
    else
        f = stdout;

    ctx = udev_new();

    enm = udev_enumerate_new(ctx);

    udev_enumerate_add_match_subsystem(enm, "hidraw");

    udev_enumerate_scan_devices(enm);

    devs = udev_enumerate_get_list_entry(enm);

    fprintf(f, "dumping dev list:\n");
    dump_list(devs);

    udev_list_entry_foreach(dev_entry, devs)
    {
        const char *path, *devnode;
        int fd;

        path = udev_list_entry_get_name(dev_entry);
        fprintf(f, "got dev path: %s\n", path);
        dev = udev_device_new_from_syspath(ctx, path);

        dump_dev_tree(dev);
    }

    return 0;
}
