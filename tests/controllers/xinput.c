#include <stdio.h>
#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windows.h"
#include "setupapi.h"
#include "hidusage.h"
#include "winioctl.h"
#include "ddk/hidsdi.h"
#include "ddk/hidclass.h"
#include "xinput.h"
#include <stdarg.h>

static FILE *f;

static LRESULT WINAPI our_wnd_proc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    switch(msg)
    {
        case WM_DEVICECHANGE:
            {
            XINPUT_STATE state;
            fprintf(f, "got device change\n");
            DWORD r = XInputGetState(0, &state);
            fprintf(f, "xinputgetstate(0) --> 0x%lx\n", r);
            break;
            }
    }
    return DefWindowProcA(hwnd, msg, wparam, lparam);
}

static void test_xi(void)
{
    XINPUT_CAPABILITIES caps;
    XINPUT_STATE st, last_state;
    XINPUT_VIBRATION vib;
    XINPUT_KEYSTROKE ks;
    DWORD r, i;
    WNDCLASSA cls;
    HWND hwnd; /*for some reason, steam only reports events if we have an actual window processing events */
    BOOL br;
    MSG msg;

#if 0
    memset(&cls, 0, sizeof(cls));
    cls.lpfnWndProc = our_wnd_proc;
    cls.hInstance = GetModuleHandleA(NULL);
    cls.hCursor = LoadCursorA(0, (LPCSTR)IDC_ARROW);
    cls.hbrBackground = GetStockObject(WHITE_BRUSH);
    cls.lpszClassName = "our_class";
    RegisterClassA(&cls);

    hwnd = CreateWindowExA(0, "our_class", "window",
            WS_CAPTION | WS_SYSMENU | WS_VISIBLE, 0, 0, 100, 100,
            GetDesktopWindow(), NULL, GetModuleHandleA(NULL), NULL);
    ShowWindow(hwnd, SW_SHOW);
#endif

    XInputEnable(TRUE);

    for(i = 0; i < 10; ++i)
    {
        r = XInputGetCapabilities(0, 0, &caps);
        if(r == ERROR_SUCCESS){
            fprintf(f, "xicaps type: 0x%hhx\n", caps.Type);
            fprintf(f, "xicaps subtype: 0x%hhx\n", caps.SubType);
            fprintf(f, "xicaps flags: 0x%hx\n", caps.Flags);
            fprintf(f, "xicaps buttons: 0x%hx\n", caps.Gamepad.wButtons);
            fprintf(f, "xicaps LT: 0x%hhx\n", caps.Gamepad.bLeftTrigger);
            fprintf(f, "xicaps RT: 0x%hhx\n", caps.Gamepad.bRightTrigger);
            fprintf(f, "xicaps LX: 0x%hx\n", caps.Gamepad.sThumbLX);
            fprintf(f, "xicaps LY: 0x%hx\n", caps.Gamepad.sThumbLY);
            fprintf(f, "xicaps RX: 0x%hx\n", caps.Gamepad.sThumbRX);
            fprintf(f, "xicaps RY: 0x%hx\n", caps.Gamepad.sThumbRY);
            break;
        }else
            fprintf(f, "no controller?\n");

        Sleep(500);
    }

    fflush(f);

    do{
#if 0
        while(PeekMessageA(&msg, hwnd, 0, 0, PM_REMOVE) != 0)
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
#endif

        r = XInputGetState(0, &st);
        if(r == ERROR_SUCCESS && (st.Gamepad.wButtons != last_state.Gamepad.wButtons)){
            fprintf(f, "got dwpacketnum: %ld\n", st.dwPacketNumber);
            fprintf(f, "got buttons: 0x%hx\n", st.Gamepad.wButtons);
            fprintf(f, "got triggers: 0x%hhx | 0x%hhx\n", st.Gamepad.bLeftTrigger, st.Gamepad.bRightTrigger);
            fprintf(f, "got ls: %hd (%hx) | %hd (%hx)\n", st.Gamepad.sThumbLX, st.Gamepad.sThumbLX, st.Gamepad.sThumbLY, st.Gamepad.sThumbLY);
            fprintf(f, "got rs: %hd | %hd\n", st.Gamepad.sThumbRX, st.Gamepad.sThumbRY);
            last_state = st;
#if 0
            if(st.Gamepad.wButtons & XINPUT_GAMEPAD_A)
                break;
#endif
        }else if(r != ERROR_SUCCESS)
            fprintf(f, "getstate err: %u\n", r);

        while((st.Gamepad.wButtons & XINPUT_GAMEPAD_START) && XInputGetKeystroke(XUSER_INDEX_ANY, 0, &ks) == ERROR_SUCCESS && !(ks.Flags & XINPUT_KEYSTROKE_REPEAT))
        {
            fprintf(f, "%lu got keystroke.vk: 0x%x\n", GetTickCount(), ks.VirtualKey);
            fprintf(f, "got keystroke.unicode: 0x%x\n", ks.Unicode);
            fprintf(f, "got keystroke.flags: 0x%x\n", ks.Flags);
            fprintf(f, "got keystroke.idx: 0x%x\n", ks.UserIndex);
            fprintf(f, "got keystroke.hid: 0x%x\n", ks.HidCode);
            fprintf(f, "with sthumblx %hd\n", st.Gamepad.sThumbLX);
        }
        fflush(f);
//        Sleep(50);
    }while(1);
}

int main(int argc, char **argv)
{
    if(argc > 1)
        f = fopen(argv[1], "w");
    else
        f = stdout;

    test_xi();
    fclose(f);
    return 0;
}
