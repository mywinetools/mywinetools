
#define HID_CTL_CODE(id) \
  CTL_CODE (FILE_DEVICE_KEYBOARD, (id), METHOD_NEITHER, FILE_ANY_ACCESS)

#define IOCTL_HID_GET_STRING                     HID_CTL_CODE(4)



#include <stdio.h>
#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windows.h"
#include "setupapi.h"
#include "hidusage.h"
#include "winioctl.h"
#include "hidpi.h"
#include "hidsdi.h"
#include "hidclass.h"
//#include "ddk/hidport.h"
#include "xinput.h"
#include <stdarg.h>

//#include "wine/test.h"

//#define READ_MAX_TIME 5000

static FILE *f;

#if 0
typedef struct _HIDP_LINK_COLLECTION_NODE {
    USAGE  LinkUsage;
    USAGE  LinkUsagePage;
    USHORT Parent;
    USHORT NumberOfChildren;
    USHORT NextSibling;
    USHORT FirstChild;
    ULONG  CollectionType : 8;
    ULONG  IsAlias : 1;
    ULONG  Reserved : 23;
    PVOID  UserContext;
} HIDP_LINK_COLLECTION_NODE, *PHIDP_LINK_COLLECTION_NODE;
#endif

static void test_my(void)
{
    RAWINPUTDEVICELIST *list;
    UINT ur, ndevs = 0, devi, i, sz;
    BOOL br;
    char path[2048];
    PHIDP_PREPARSED_DATA parsed;
    HIDP_CAPS caps;
    HIDP_BUTTON_CAPS *button_caps;
    HIDP_VALUE_CAPS *value_caps;
    HIDP_LINK_COLLECTION_NODE *link_nodes;
    HIDP_DATA *data_list;
    USHORT nbutton_caps;
    USHORT nvalue_caps;
    ULONG nlink_nodes;
    USHORT ntries;
    ULONG ndata_list;
    HANDLE dev;
    NTSTATUS nts;
    DWORD junk;
    WCHAR string[2048];
    char stringA[2048];
    LONGLONG last;

    ur = GetRawInputDeviceList(NULL, &ndevs, sizeof(*list));
    if(!(ur != -1)) fprintf(f,  "GRIDL failed\n");

    if(!(0)) fprintf(f,  "got %u devs\n", ndevs);

    list = HeapAlloc(GetProcessHeap(), 0, ndevs);

    ur = GetRawInputDeviceList(list, &ndevs, sizeof(*list));
    if(!(ur != -1)) fprintf(f,  "GRIDL failed\n");

    for(devi = 0; devi < ndevs; ++devi){
        if(list[devi].dwType == RIM_TYPEHID){
            sz = sizeof(path) / sizeof(*path);
            ur = GetRawInputDeviceInfoA(list[devi].hDevice, RIDI_DEVICENAME,
                    path, &sz);
            if(!(ur != -1)) fprintf(f,  "GRIDI failed\n");

            if(!(0)) fprintf(f,  "got hid device path: %s\n",path);
            if(strstr(path, "WINEMOUSE"))
                continue;

            dev = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
                    NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
            if(!(dev != INVALID_HANDLE_VALUE))
            {
                fprintf(f,  "CreateFile failed\n");
                return;
            }

            memset(string, 0, sizeof(string));
            DeviceIoControl(dev, 0xb01be, NULL, 0, string, sizeof(string), &junk, NULL);
            WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
            fprintf(f, "got device string: %s\n", stringA);

            HIDD_ATTRIBUTES attr;
            HidD_GetAttributes(dev, &attr);
            fprintf(f, "got vid 0x%hx, pid 0x%hx, ver 0x%hx\n", attr.VendorID, attr.ProductID, attr.VersionNumber);

            memset(string, 0, sizeof(string));
            if(HidD_GetManufacturerString(dev, string, sizeof(string)))
            {
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got manufacturer string: %s\n", stringA);
            }else
                fprintf(f, "GetManufacturerString failed: 0x%x\n", GetLastError());

            memset(string, 0, sizeof(string));
            if(HidD_GetProductString(dev, string, sizeof(string)))
            {
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got product string: %s\n", stringA);
            }else
                fprintf(f, "GetProductString failed: 0x%x\n", GetLastError());

            memset(string, 0, sizeof(string));
            if(HidD_GetSerialNumberString(dev, string, sizeof(string))){
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got serial number string: %s\n", stringA);
            }else
                fprintf(f, "GetSerialNumberString failed: 0x%x\n", GetLastError());

            br = HidD_GetPreparsedData(dev, &parsed);
            fprintf(f, "a\n");
            if(!(br == TRUE))
            {
                fprintf(f,  "GetPreparsedData failed\n");
                CloseHandle(dev);
                return;
            }

            fprintf(f, "b\n");
            br = HidP_GetCaps(parsed, &caps);
            fprintf(f, "c\n");
            if(!(br == HIDP_STATUS_SUCCESS)) fprintf(f,  "GetCaps failed\n");

            fprintf(f, "Usage: %u\n", caps.Usage);
            fprintf(f, "UsagePage: %u\n", caps.UsagePage);
            fprintf(f, "InputReportByteLength: %hu\n", caps.InputReportByteLength);
            fprintf(f, "OutputReportByteLength: %hu\n", caps.OutputReportByteLength);
            fprintf(f, "FeatureReportByteLength: %hu\n", caps.FeatureReportByteLength);
            //fprintf(f, "Reserved: %hu\n", caps.Reserved);
            fprintf(f, "NumberLinkCollectionNodes: %hu\n", caps.NumberLinkCollectionNodes);
            fprintf(f, "NumberInputButtonCaps: %hu\n", caps.NumberInputButtonCaps);
            fprintf(f, "NumberInputValueCaps: %hu\n", caps.NumberInputValueCaps);
            fprintf(f, "NumberInputDataIndices: %hu\n", caps.NumberInputDataIndices);
            fprintf(f, "NumberOutputButtonCaps: %hu\n", caps.NumberOutputButtonCaps);
            fprintf(f, "NumberOutputValueCaps: %hu\n", caps.NumberOutputValueCaps);
            fprintf(f, "NumberOutputDataIndices: %hu\n", caps.NumberOutputDataIndices);
            fprintf(f, "NumberFeatureButtonCaps: %hu\n", caps.NumberFeatureButtonCaps);
            fprintf(f, "NumberFeatureValueCaps: %hu\n", caps.NumberFeatureValueCaps);
            fprintf(f, "NumberFeatureDataIndices: %hu\n", caps.NumberFeatureDataIndices);

            nlink_nodes = caps.NumberLinkCollectionNodes;
            link_nodes = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, nlink_nodes * sizeof(*link_nodes));

            br = HidP_GetLinkCollectionNodes(link_nodes, &nlink_nodes, parsed);
            if(!(br == HIDP_STATUS_SUCCESS)) fprintf(f, "GetLinkCollectionNodes failed\n");

            for(i = 0; i < nlink_nodes; ++i){
                fprintf(f, "%u, LinkUsage: %hu\n", i, link_nodes[i].LinkUsage);
                fprintf(f, "%u, LinkUsagePage: %hu\n", i, link_nodes[i].LinkUsagePage);
                fprintf(f, "%u, Parent: %hu\n", i, link_nodes[i].Parent);
                fprintf(f, "%u, NumberOfChildren: %hu\n", i, link_nodes[i].NumberOfChildren);
                fprintf(f, "%u, NextSibling: %hu\n", i, link_nodes[i].NextSibling);
                fprintf(f, "%u, FirstChild: %hu\n", i, link_nodes[i].FirstChild);
                fprintf(f, "%u, CollectionType: %u\n", i, link_nodes[i].CollectionType);
                fprintf(f, "%u, IsAlias: %u\n", i, link_nodes[i].IsAlias);
                fprintf(f, "%u, UserContext: %p\n", i, link_nodes[i].UserContext);
                fprintf(f, "\n");
            }

            nbutton_caps = caps.NumberInputButtonCaps;
            button_caps = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, nbutton_caps * sizeof(*button_caps));

            br = HidP_GetButtonCaps(HidP_Input, button_caps, &nbutton_caps, parsed);
            if(!(br == HIDP_STATUS_SUCCESS)) fprintf(f,  "GetButtonCaps failed\n");

            for(i = 0; i < nbutton_caps; ++i){
                fprintf(f, "%u, UsagePage: %hu\n", i, button_caps[i].UsagePage);
                fprintf(f, "%u, ReportID: %hhu\n", i, button_caps[i].ReportID);
                fprintf(f, "%u, IsAlias: %hhu\n", i, button_caps[i].IsAlias);
                fprintf(f, "%u, BitField: %hu\n", i, button_caps[i].BitField);
                fprintf(f, "%u, LinkCollection: %hu\n", i, button_caps[i].LinkCollection);
                fprintf(f, "%u, LinkUsage: %hu\n", i, button_caps[i].LinkUsage);
                fprintf(f, "%u, LinkUsagePage: %hu\n", i, button_caps[i].LinkUsagePage);
                fprintf(f, "%u, IsRange: %hhu\n", i, button_caps[i].IsRange);
                fprintf(f, "%u, IsStringRange: %hhu\n", i, button_caps[i].IsStringRange);
                fprintf(f, "%u, IsDesignatorRange: %hhu\n", i, button_caps[i].IsDesignatorRange);
                fprintf(f, "%u, IsAbsolute: %hhu\n", i, button_caps[i].IsAbsolute);
                    //ULONG    Reserved[10];
                if(button_caps[i].IsRange){
                    fprintf(f, "%u, UsageMin: %hu\n", i, button_caps[i].Range.UsageMin);
                    fprintf(f, "%u, UsageMax: %hu\n", i, button_caps[i].Range.UsageMax);
                    fprintf(f, "%u, StringMin: %hu\n", i, button_caps[i].Range.StringMin);
                    fprintf(f, "%u, StringMax: %hu\n", i, button_caps[i].Range.StringMax);
                    fprintf(f, "%u, DesignatorMin: %hu\n", i, button_caps[i].Range.DesignatorMin);
                    fprintf(f, "%u, DesignatorMax: %hu\n", i, button_caps[i].Range.DesignatorMax);
                    fprintf(f, "%u, DataIndexMin: %hu\n", i, button_caps[i].Range.DataIndexMin);
                    fprintf(f, "%u, DataIndexMax: %hu\n", i, button_caps[i].Range.DataIndexMax);
                }else{
                    fprintf(f, "%u, Usage: %hu\n", i, button_caps[i].NotRange.Usage);
                    fprintf(f, "%u, Reserved1: %hu\n", i, button_caps[i].NotRange.Reserved1);
                    fprintf(f, "%u, StringIndex: %hu\n", i, button_caps[i].NotRange.StringIndex);
                    fprintf(f, "%u, Reserved2: %hu\n", i, button_caps[i].NotRange.Reserved2);
                    fprintf(f, "%u, DesignatorIndex: %hu\n", i, button_caps[i].NotRange.DesignatorIndex);
                    fprintf(f, "%u, Reserved3: %hu\n", i, button_caps[i].NotRange.Reserved3);
                    fprintf(f, "%u, DataIndex: %hu\n", i, button_caps[i].NotRange.DataIndex);
                    fprintf(f, "%u, Reserved4: %hu\n", i, button_caps[i].NotRange.Reserved4);
                }
                fprintf(f, "\n");
            }

            nvalue_caps = caps.NumberInputValueCaps;
            value_caps = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, nvalue_caps * sizeof(*value_caps));
            br = HidP_GetValueCaps(HidP_Input, value_caps, &nvalue_caps, parsed);
            if(!(br == HIDP_STATUS_SUCCESS)) fprintf(f,  "GetValueCaps failed\n");

            for(i = 0; i < nvalue_caps; ++i){
                fprintf(f, "%u, UsagePage: %hu\n", i, value_caps[i].UsagePage);
                fprintf(f, "%u, ReportID: %hhu\n", i, value_caps[i].ReportID);
                fprintf(f, "%u, IsAlias: %hhu\n", i, value_caps[i].IsAlias);
                fprintf(f, "%u, BitField: %hu\n", i, value_caps[i].BitField);
                fprintf(f, "%u, LinkCollection: %hu\n", i, value_caps[i].LinkCollection);
                fprintf(f, "%u, LinkUsage: %hu\n", i, value_caps[i].LinkUsage);
                fprintf(f, "%u, LinkUsagePage: %hu\n", i, value_caps[i].LinkUsagePage);
                fprintf(f, "%u, IsRange: %hhu\n", i, value_caps[i].IsRange);
                fprintf(f, "%u, IsStringRange: %hhu\n", i, value_caps[i].IsStringRange);
                fprintf(f, "%u, IsDesignatorRange: %hhu\n", i, value_caps[i].IsDesignatorRange);
                fprintf(f, "%u, IsAbsolute: %hhu\n", i, value_caps[i].IsAbsolute);
                fprintf(f, "%u, HasNull: %hhu\n", i, value_caps[i].HasNull);
                fprintf(f, "%u, Reserved: %hhu\n", i, value_caps[i].Reserved);
                fprintf(f, "%u, BitSize: %hu\n", i, value_caps[i].BitSize);
                fprintf(f, "%u, ReportCount: %hu\n", i, value_caps[i].ReportCount);
                //fprintf(f, "%u, Reserved2: %hu\n", i, value_caps[i].Reserved2)[5];
                fprintf(f, "%u, UnitsExp: %u\n", i, value_caps[i].UnitsExp);
                fprintf(f, "%u, Units: %u\n", i, value_caps[i].Units);
                fprintf(f, "%u, LogicalMin: %u\n", i, value_caps[i].LogicalMin);
                fprintf(f, "%u, LogicalMax: %u\n", i, value_caps[i].LogicalMax);
                fprintf(f, "%u, PhysicalMin: %u\n", i, value_caps[i].PhysicalMin);
                fprintf(f, "%u, PhysicalMax: %u\n", i, value_caps[i].PhysicalMax);

                if(value_caps[i].IsRange){
                    fprintf(f, "%u, UsageMin: %hu\n", i, value_caps[i].Range.UsageMin);
                    fprintf(f, "%u, UsageMax: %hu\n", i, value_caps[i].Range.UsageMax);
                    fprintf(f, "%u, StringMin: %hu\n", i, value_caps[i].Range.StringMin);
                    fprintf(f, "%u, StringMax: %hu\n", i, value_caps[i].Range.StringMax);
                    fprintf(f, "%u, DesignatorMin: %hu\n", i, value_caps[i].Range.DesignatorMin);
                    fprintf(f, "%u, DesignatorMax: %hu\n", i, value_caps[i].Range.DesignatorMax);
                    fprintf(f, "%u, DataIndexMin: %hu\n", i, value_caps[i].Range.DataIndexMin);
                    fprintf(f, "%u, DataIndexMax: %hu\n", i, value_caps[i].Range.DataIndexMax);
                }else{
                    fprintf(f, "%u, Usage: %hu\n", i, value_caps[i].NotRange.Usage);
                    fprintf(f, "%u, Reserved1: %hu\n", i, value_caps[i].NotRange.Reserved1);
                    fprintf(f, "%u, StringIndex: %hu\n", i, value_caps[i].NotRange.StringIndex);
                    fprintf(f, "%u, Reserved2: %hu\n", i, value_caps[i].NotRange.Reserved2);
                    fprintf(f, "%u, DesignatorIndex: %hu\n", i, value_caps[i].NotRange.DesignatorIndex);
                    fprintf(f, "%u, Reserved3: %hu\n", i, value_caps[i].NotRange.Reserved3);
                    fprintf(f, "%u, DataIndex: %hu\n", i, value_caps[i].NotRange.DataIndex);
                    fprintf(f, "%u, Reserved4: %hu\n", i, value_caps[i].NotRange.Reserved4);
                }
                fprintf(f, "\n");
            }

            nvalue_caps = caps.NumberFeatureValueCaps;
            value_caps = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, nvalue_caps * sizeof(*value_caps));
            br = HidP_GetValueCaps(HidP_Feature, value_caps, &nvalue_caps, parsed);
            if(!(br == HIDP_STATUS_SUCCESS)) fprintf(f,  "GetValueCaps failed\n");

            for(i = 0; i < nvalue_caps; ++i){
                fprintf(f, "%u, UsagePage: %hu\n", i, value_caps[i].UsagePage);
                fprintf(f, "%u, ReportID: %hhu\n", i, value_caps[i].ReportID);
                fprintf(f, "%u, IsAlias: %hhu\n", i, value_caps[i].IsAlias);
                fprintf(f, "%u, BitField: %hu\n", i, value_caps[i].BitField);
                fprintf(f, "%u, LinkCollection: %hu\n", i, value_caps[i].LinkCollection);
                fprintf(f, "%u, LinkUsage: %hu\n", i, value_caps[i].LinkUsage);
                fprintf(f, "%u, LinkUsagePage: %hu\n", i, value_caps[i].LinkUsagePage);
                fprintf(f, "%u, IsRange: %hhu\n", i, value_caps[i].IsRange);
                fprintf(f, "%u, IsStringRange: %hhu\n", i, value_caps[i].IsStringRange);
                fprintf(f, "%u, IsDesignatorRange: %hhu\n", i, value_caps[i].IsDesignatorRange);
                fprintf(f, "%u, IsAbsolute: %hhu\n", i, value_caps[i].IsAbsolute);
                fprintf(f, "%u, HasNull: %hhu\n", i, value_caps[i].HasNull);
                fprintf(f, "%u, Reserved: %hhu\n", i, value_caps[i].Reserved);
                fprintf(f, "%u, BitSize: %hu\n", i, value_caps[i].BitSize);
                fprintf(f, "%u, ReportCount: %hu\n", i, value_caps[i].ReportCount);
                //fprintf(f, "%u, Reserved2: %hu\n", i, value_caps[i].Reserved2)[5];
                fprintf(f, "%u, UnitsExp: %u\n", i, value_caps[i].UnitsExp);
                fprintf(f, "%u, Units: %u\n", i, value_caps[i].Units);
                fprintf(f, "%u, LogicalMin: %u\n", i, value_caps[i].LogicalMin);
                fprintf(f, "%u, LogicalMax: %u\n", i, value_caps[i].LogicalMax);
                fprintf(f, "%u, PhysicalMin: %u\n", i, value_caps[i].PhysicalMin);
                fprintf(f, "%u, PhysicalMax: %u\n", i, value_caps[i].PhysicalMax);

                if(value_caps[i].IsRange){
                    fprintf(f, "%u, UsageMin: %hu\n", i, value_caps[i].Range.UsageMin);
                    fprintf(f, "%u, UsageMax: %hu\n", i, value_caps[i].Range.UsageMax);
                    fprintf(f, "%u, StringMin: %hu\n", i, value_caps[i].Range.StringMin);
                    fprintf(f, "%u, StringMax: %hu\n", i, value_caps[i].Range.StringMax);
                    fprintf(f, "%u, DesignatorMin: %hu\n", i, value_caps[i].Range.DesignatorMin);
                    fprintf(f, "%u, DesignatorMax: %hu\n", i, value_caps[i].Range.DesignatorMax);
                    fprintf(f, "%u, DataIndexMin: %hu\n", i, value_caps[i].Range.DataIndexMin);
                    fprintf(f, "%u, DataIndexMax: %hu\n", i, value_caps[i].Range.DataIndexMax);
                }else{
                    fprintf(f, "%u, Usage: %hu\n", i, value_caps[i].NotRange.Usage);
                    fprintf(f, "%u, Reserved1: %hu\n", i, value_caps[i].NotRange.Reserved1);
                    fprintf(f, "%u, StringIndex: %hu\n", i, value_caps[i].NotRange.StringIndex);
                    fprintf(f, "%u, Reserved2: %hu\n", i, value_caps[i].NotRange.Reserved2);
                    fprintf(f, "%u, DesignatorIndex: %hu\n", i, value_caps[i].NotRange.DesignatorIndex);
                    fprintf(f, "%u, Reserved3: %hu\n", i, value_caps[i].NotRange.Reserved3);
                    fprintf(f, "%u, DataIndex: %hu\n", i, value_caps[i].NotRange.DataIndex);
                    fprintf(f, "%u, Reserved4: %hu\n", i, value_caps[i].NotRange.Reserved4);
                }
                fprintf(f, "\n");
            }





            ndata_list = HidP_MaxDataListLength(HidP_Input, parsed);
            if(!(0)) fprintf(f,  "got max data length: 0x%x\n", ndata_list);

            data_list = HeapAlloc(GetProcessHeap(), 0, ndata_list * sizeof(HIDP_DATA));

            last = 0;
            while(1){
                DWORD tx;
                OVERLAPPED overlapped;
                unsigned char dat[512];
                ULONG len;
                DWORD junk;

                memset(&overlapped, 0, sizeof(overlapped));
                memset(dat, 0, sizeof(dat));

                br = ReadFile(dev, dat, sizeof(dat), &junk, &overlapped);
//                if(!(br == TRUE)) fprintf(f,  "ReadFile should have given TRUE\n");

                while(br == FALSE){
        //            if(!(0)) fprintf(f,  "sleeping\n");
                    Sleep(100);
                    br = GetOverlappedResult(dev, &overlapped, &tx, FALSE);
                    junk = tx;
                }


              if(GetTickCount() > last + 500){
                last = GetTickCount();
                fprintf(f, "got %u read\n", junk);
                for(i = 0; i < junk; ++i)
                {
                    fprintf(f, "%02hhx ", dat[i]);
                }
                fprintf(f, "\n");
                /*
                fprintf(f, "%02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx\n",
                        dat[0],
                        dat[1],
                        dat[2],
                        dat[3],
                        dat[4],
                        dat[5],
                        dat[6],
                        dat[7],
                        dat[8],
                        dat[9],
                        dat[10],
                        dat[11],
                        dat[12],
                        dat[13],
                        dat[14]);
                        */
                fflush(stdout);

        //        if(!(0)) fprintf(f,  "--------------------\n");
                if(br){
                    len = ndata_list;
                    memset(data_list, 0, sizeof(*data_list) * ndata_list);
                    nts = HidP_GetData(HidP_Input, data_list, &len, parsed, dat, junk);
                    if(!(nts == HIDP_STATUS_SUCCESS)) fprintf(f,  "GetData failed\n");

                    for(i = 0; i < ndata_list && i < 10; ++i){
                        fprintf(f,  "got data[%u]: %u --> 0x%x\n", i, data_list[i].DataIndex, data_list[i].RawValue);
                    }
                    fflush(stdout);
                }
              }
                Sleep(10);
            }

            HidD_FreePreparsedData(parsed);

            CloseHandle(dev);
        }
    }
}

static LRESULT WINAPI our_wnd_proc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    switch(msg)
    {
        case WM_DEVICECHANGE:
            {
            XINPUT_STATE state;
            fprintf(f, "got device change\n");
            DWORD r = XInputGetState(0, &state);
            fprintf(f, "xinputgetstate(0) --> 0x%x\n", r);
            break;
            }
    }
    return DefWindowProcA(hwnd, msg, wparam, lparam);
}

static void test_xi(void)
{
    XINPUT_CAPABILITIES caps;
    XINPUT_STATE st;
    XINPUT_VIBRATION vib;
    DWORD r, i;
    WNDCLASSA cls;
    HWND hwnd; /*for some reason, steam only reports events if we have an actual window processing events */
    BOOL br;
    MSG msg;

#if 0
    memset(&cls, 0, sizeof(cls));
    cls.lpfnWndProc = our_wnd_proc;
    cls.hInstance = GetModuleHandleA(NULL);
    cls.hCursor = LoadCursorA(0, (LPCSTR)IDC_ARROW);
    cls.hbrBackground = GetStockObject(WHITE_BRUSH);
    cls.lpszClassName = "our_class";
    RegisterClassA(&cls);

    hwnd = CreateWindowExA(0, "our_class", "window",
            WS_CAPTION | WS_SYSMENU | WS_VISIBLE, 0, 0, 100, 100,
            GetDesktopWindow(), NULL, GetModuleHandleA(NULL), NULL);
    ShowWindow(hwnd, SW_SHOW);
    RegisterDeviceNotificationW(hwnd, NULL, 0);
#endif

#if 0
    XInputEnable(TRUE);

    for(i = 0; i < 10; ++i)
    {
        r = XInputGetCapabilities(0, 0, &caps);
        if(r == ERROR_SUCCESS){
            fprintf(f, "xicaps type: 0x%hhx\n", caps.Type);
            fprintf(f, "xicaps subtype: 0x%hhx\n", caps.SubType);
            fprintf(f, "xicaps flags: 0x%hx\n", caps.Flags);
            fprintf(f, "xicaps buttons: 0x%hx\n", caps.Gamepad.wButtons);
            fprintf(f, "xicaps LT: 0x%hhx\n", caps.Gamepad.bLeftTrigger);
            fprintf(f, "xicaps RT: 0x%hhx\n", caps.Gamepad.bRightTrigger);
            fprintf(f, "xicaps LX: 0x%hx\n", caps.Gamepad.sThumbLX);
            fprintf(f, "xicaps LY: 0x%hx\n", caps.Gamepad.sThumbLY);
            fprintf(f, "xicaps RX: 0x%hx\n", caps.Gamepad.sThumbRX);
            fprintf(f, "xicaps RY: 0x%hx\n", caps.Gamepad.sThumbRY);
            break;
        }

        Sleep(500);
    }

    vib.wLeftMotorSpeed = 65535;
    vib.wRightMotorSpeed = 65535;
#endif

    do{
#if 0
        while(PeekMessageA(&msg, hwnd, 0, 0, PM_REMOVE) != 0)
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

#else
        r = XInputGetState(0, &st);
        if(r == ERROR_SUCCESS){
            fprintf(f, "got dwpacketnum: %d\n", st.dwPacketNumber);
            fprintf(f, "got buttons: 0x%hx\n", st.Gamepad.wButtons);
            fprintf(f, "got triggers: 0x%hhx | 0x%hhx\n", st.Gamepad.bLeftTrigger, st.Gamepad.bRightTrigger);
            fprintf(f, "got ls: %hd (%hx) | %hd (%hx)\n", st.Gamepad.sThumbLX, st.Gamepad.sThumbLX, st.Gamepad.sThumbLY, st.Gamepad.sThumbLY);
            fprintf(f, "got rs: %hd | %hd\n", st.Gamepad.sThumbRX, st.Gamepad.sThumbRY);
            if(st.Gamepad.wButtons & XINPUT_GAMEPAD_A)
                break;
        }else
            fprintf(f, "no gamepad\n");
        //XInputSetState(0, &vib);
        Sleep(500);
#endif
    }while(1);

#if 0
    vib.wLeftMotorSpeed = 0;
    vib.wRightMotorSpeed = 0;

    XInputSetState(0, &vib);
#endif
}

int main(int argc, char **argv)
{
    if(argc > 1)
        f = fopen(argv[1], "w");
    else
        f = stdout;

    //test_xi();
    test_my();
    fclose(f);
    return 0;
}
