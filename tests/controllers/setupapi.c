#include <stdio.h>
#include <windows.h>
#include <setupapi.h>
#include <hidsdi.h>

static FILE *f;
//#include <ddk/hidclass.h>
static GUID GUID_DEVINTERFACE_HID = {
    0x4d1e55b2,
    0xf16f, 0x11cf,
    {0x88, 0xcb,
    0x00,
        0x11,
        0x11,
        0x00,
        0x00,
        0x30
    }
};

static const char *debugstr_guid(GUID *id)
{
    static char s[256];
    sprintf(s, "{%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x}",
                             id->Data1, id->Data2, id->Data3,
                             id->Data4[0], id->Data4[1], id->Data4[2], id->Data4[3],
                             id->Data4[4], id->Data4[5], id->Data4[6], id->Data4[7]);
    return s;
}

int main(int argc, char **argv)
{
    HDEVINFO devinfo;
    SP_DEVICE_INTERFACE_DATA data;
    SP_DEVICE_INTERFACE_DETAIL_DATA_A *detail;
    BOOL br;
    DWORD i = 0, req;

    if(argc > 1)
        f = fopen(argv[1], "w");
    else
        f = stdout;

    fprintf(f, "setupapi started\n");

    devinfo = SetupDiGetClassDevsExA(&GUID_DEVINTERFACE_HID, NULL, 0,
            DIGCF_PRESENT | DIGCF_ALLCLASSES | DIGCF_DEVICEINTERFACE, 0, NULL,
            0);
    if(devinfo == INVALID_HANDLE_VALUE)
    {
        fprintf(f, "SetupDiGetClassDevsExA failed\n");
        fclose(f);
        return 1;
    }

    while(1)
    {
        memset(&data, 0, sizeof(data));
        data.cbSize = sizeof(data);

        br = SetupDiEnumDeviceInterfaces(devinfo, NULL, &GUID_DEVINTERFACE_HID, i++, &data);
        if(!br)
        {
            fprintf(f, "done enumerating\n");
            break;
        }

        fprintf(f, "got one, guid: %s, flags: %08x, reserved: %p\n",
                debugstr_guid(&data.InterfaceClassGuid), data.Flags, (void*)data.Reserved);

        req = 0;
        br = SetupDiGetDeviceInterfaceDetailA(devinfo, &data, NULL, 0, &req, NULL);
        if(req == 0)
        {
            fprintf(f, "GDID1 failed\n");
            break;
        }

        detail = HeapAlloc(GetProcessHeap(), 0, req);
        detail->cbSize = sizeof(*detail);

        br = SetupDiGetDeviceInterfaceDetailA(devinfo, &data, detail, req, &req, NULL);
        if(!br)
        {
            fprintf(f, "GDID2 failed\n");
            break;
        }

        fprintf(f, "got device path: %s\n", detail->DevicePath);

        HANDLE d = CreateFileA(detail->DevicePath,0xc0000000,3,(LPSECURITY_ATTRIBUTES)0x0,3,
                    0,(HANDLE)0x0);
        HIDD_ATTRIBUTES attr;
        HidD_GetAttributes(d, &attr);
        fprintf(f, "got vid 0x%hx, pid 0x%hx, ver 0x%hx\n", attr.VendorID, attr.ProductID, attr.VersionNumber);

//        HidD_GetPreparsedData(d, &preparsed);
    }

    fclose(f);

    return 0;
}
