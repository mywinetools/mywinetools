#include <windows.h>

#define COBJMACROS
#include <xinput.h>
#include <initguid.h>
#include <dinput.h>
#include <wbemidl.h>
#include <wbemdisp.h>
#include <oleauto.h>
#include <unknwn.h>
#include "ddk/hidsdi.h"
#include "ddk/hidclass.h"

#include <stdio.h>

static IDirectInput *di;
static FILE *f;

static const char *debugstr_guid(const GUID *id)
{
    static char s[256];
    sprintf(s, "{%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x}",
                             id->Data1, id->Data2, id->Data3,
                             id->Data4[0], id->Data4[1], id->Data4[2], id->Data4[3],
                             id->Data4[4], id->Data4[5], id->Data4[6], id->Data4[7]);
    return s;
}

BOOL obj_cb(const DIDEVICEOBJECTINSTANCEA *obj, void *user)
{
    fprintf(f, "got object:\n");
    fprintf(f, "\toffs: %ld\n", obj->dwOfs);
    fprintf(f, "\ttype: 0x%08lx\n", obj->dwType);
    fprintf(f, "\tflags: 0x%08lx\n", obj->dwFlags);
    fprintf(f, "\tname: %s\n", obj->tszName);
    fprintf(f, "\tguidtype: %s\n", debugstr_guid(&obj->guidType));

    return DIENUM_CONTINUE;
}

BOOL IsXInputDevice( const GUID* pGuidProductFromDirectInput )
{
    IWbemLocator*           pIWbemLocator  = NULL;
    IEnumWbemClassObject*   pEnumDevices   = NULL;
    IWbemClassObject*       pDevices[20]   = {0};
    IWbemServices*          pIWbemServices = NULL;
    BSTR                    bstrNamespace  = NULL;
    BSTR                    bstrDeviceID   = NULL;
    BSTR                    bstrClassName  = NULL;
    DWORD                   uReturned      = 0;
    BOOL                    bIsXinputDevice= FALSE;
    UINT                    iDevice        = 0;
    VARIANT                 var;
    HRESULT                 hr;

    fprintf(f, "in IsXInputDevice\n");
    fflush(f);

    // CoInit if needed
    hr = CoInitialize(NULL);
    BOOL bCleanupCOM = SUCCEEDED(hr);

    // Create WMI
    hr = CoCreateInstance( &CLSID_WbemLocator,
                           NULL,
                           CLSCTX_INPROC_SERVER,
                           &IID_IWbemLocator,
                           (LPVOID*) &pIWbemLocator);
    if( FAILED(hr) || pIWbemLocator == NULL )
        goto LCleanup;
    fprintf(f, "a\n"); fflush(f);

    bstrNamespace = SysAllocString( L"\\\\.\\root\\cimv2" );if( bstrNamespace == NULL ) goto LCleanup;
    bstrClassName = SysAllocString( L"Win32_PNPEntity" );   if( bstrClassName == NULL ) goto LCleanup;
    bstrDeviceID  = SysAllocString( L"DeviceID" );          if( bstrDeviceID == NULL )  goto LCleanup;

    // Connect to WMI
    hr = IWbemLocator_ConnectServer( pIWbemLocator, bstrNamespace, NULL, NULL, 0L,
                                       0L, NULL, NULL, &pIWbemServices );
    if( FAILED(hr) || pIWbemServices == NULL )
        goto LCleanup;

    fprintf(f, "b\n"); fflush(f);
    // Switch security level to IMPERSONATE.
    CoSetProxyBlanket( (IUnknown*)pIWbemServices, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL,
                       RPC_C_AUTHN_LEVEL_CALL, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE );

    hr = IWbemServices_CreateInstanceEnum( pIWbemServices, bstrClassName, 0, NULL, &pEnumDevices );
    if( FAILED(hr) || pEnumDevices == NULL )
        goto LCleanup;

    fprintf(f, "c\n"); fflush(f);
    // Loop over all devices
    for( ;; )
    {
        // Get 20 at a time
    fprintf(f, "d\n"); fflush(f);
        hr = IEnumWbemClassObject_Next( pEnumDevices, 10000, 20, pDevices, &uReturned );
        if( FAILED(hr) )
            goto LCleanup;
        if( uReturned == 0 )
            break;

    fprintf(f, "e\n"); fflush(f);
        for( iDevice=0; iDevice<uReturned; iDevice++ )
        {
            // For each device, get its device ID
            hr = IWbemClassObject_Get(pDevices[iDevice], bstrDeviceID, 0L, &var, NULL, NULL );
    fprintf(f, "f\n"); fflush(f);
            if( SUCCEEDED( hr ) && var.vt == VT_BSTR && var.bstrVal != NULL )
            {
                fprintf(f, "got %S\n", var.bstrVal);
                // Check if the device ID contains "IG_".  If it does, then it's an XInput device
                    // This information can not be found from DirectInput
                if( wcsstr( var.bstrVal, L"IG_" ) )
                {
                    // If it does, then get the VID/PID from var.bstrVal
                    DWORD dwPid = 0, dwVid = 0;
                    WCHAR* strVid = wcsstr( var.bstrVal, L"VID_" );
                    if( strVid && swscanf( strVid, L"VID_%4X", &dwVid ) != 1 )
                        dwVid = 0;
                    WCHAR* strPid = wcsstr( var.bstrVal, L"PID_" );
                    if( strPid && swscanf( strPid, L"PID_%4X", &dwPid ) != 1 )
                        dwPid = 0;

                    // Compare the VID/PID to the DInput device
                    DWORD dwVidPid = MAKELONG( dwVid, dwPid );
                    if( dwVidPid == pGuidProductFromDirectInput->Data1 )
                    {
                        bIsXinputDevice = TRUE;
                        goto LCleanup;
                    }
                }
            }
//            IWbemClassObject_Release( pDevices[iDevice] );
        }
    }

LCleanup:
    fprintf(f, "g\n"); fflush(f);
    if(bstrNamespace)
        SysFreeString(bstrNamespace);
    if(bstrDeviceID)
        SysFreeString(bstrDeviceID);
    if(bstrClassName)
        SysFreeString(bstrClassName);
#if 0
    for( iDevice=0; iDevice<20; iDevice++ )
        IWbemClassObject_Release( (IUnknown*) pDevices[iDevice] );
    IEnumWbemClassObject_Release( (IUnknown*) pEnumDevices );
    IWbemLocator_Release( (IUnknown*) pIWbemLocator );
    IWbemServices_Release( (IUnknown*) pIWbemServices );

    if( bCleanupCOM )
        CoUninitialize();
#endif

    return bIsXinputDevice;
}

BOOL di_cb(const DIDEVICEINSTANCEA *ddi, void *user)
{
    HRESULT hr;
    IDirectInputDevice *device;
    DIPROPDWORD prop;
    DIPROPGUIDANDPATH gnp;
    DIJOYSTATE2 dat, last;
    int i;
    HANDLE hid;
    HIDD_ATTRIBUTES attr;
    WCHAR string[512];
    char stringA[512];

    fprintf(f, "got size: 0x%x\n", ddi->dwSize);
    fprintf(f, "got instance guid: %s\n", debugstr_guid(&ddi->guidInstance));
    fprintf(f, "got product guid: %s\n", debugstr_guid(&ddi->guidProduct));
    fprintf(f, "got devtype: 0x%x\n", ddi->dwDevType);
    fprintf(f, "got instance name: %s\n", ddi->tszInstanceName);
    fprintf(f, "got product name: %s\n", ddi->tszProductName);
    fprintf(f, "got ffdriver: %s\n", debugstr_guid(&ddi->guidFFDriver));
    fprintf(f, "got usage page/usage: 0x%hx, 0x%hx\n", ddi->wUsagePage, ddi->wUsage);
    fflush(f);

    hr = IDirectInput_CreateDevice(di, &ddi->guidInstance, &device, NULL);
    if(hr == S_OK)
    {
        memset(&gnp, 0, sizeof(gnp));
        gnp.diph.dwSize = sizeof(gnp);
        gnp.diph.dwHeaderSize = sizeof(gnp.diph);
        gnp.diph.dwObj = 0;
        gnp.diph.dwHow = DIPH_DEVICE;

        hr = IDirectInputDevice_GetProperty(device, DIPROP_GUIDANDPATH, &gnp.diph);
        if(hr == S_OK)
        {
            WideCharToMultiByte(CP_ACP, 0, gnp.wszPath, -1, stringA, sizeof(stringA), 0, NULL);
            fprintf(f, "Got GNP path: %s\n", stringA);
        }
        else
            fprintf(f, "GetProperty(GNP) failed: %08lx\n", hr);

        hid = CreateFileW(gnp.wszPath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
        if(hid != INVALID_HANDLE_VALUE)
        {
            HidD_GetAttributes(hid, &attr);
            fprintf(f, "got vid 0x%hx, pid 0x%hx, ver 0x%hx\n", attr.VendorID, attr.ProductID, attr.VersionNumber);

            memset(string, 0, sizeof(string));
            if(HidD_GetManufacturerString(hid, string, sizeof(string)))
            {
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got manufacturer string: %s\n", stringA);
            }else
                fprintf(f, "GetManufacturerString failed: 0x%x\n", GetLastError());

            memset(string, 0, sizeof(string));
            if(HidD_GetProductString(hid, string, sizeof(string)))
            {
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got product string: %s\n", stringA);
            }else
                fprintf(f, "GetProductString failed: 0x%x\n", GetLastError());

            memset(string, 0, sizeof(string));
            if(HidD_GetSerialNumberString(hid, string, sizeof(string))){
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got serial number string: %s\n", stringA);
            }else
                fprintf(f, "GetSerialNumberString failed: 0x%x\n", GetLastError());

            CloseHandle(hid);
        }else
            fprintf(f, "opening file path failed\n");

        memset(&prop, 0, sizeof(prop));
        prop.diph.dwSize = sizeof(prop);
        prop.diph.dwHeaderSize = sizeof(prop.diph);
        prop.diph.dwObj = 0;
        prop.diph.dwHow = DIPH_DEVICE;

        hr = IDirectInputDevice_GetProperty(device, DIPROP_VIDPID, &prop.diph);
        if(hr == S_OK)
            fprintf(f, "Got vidpid: %08lx\n", prop.dwData);
        else
            fprintf(f, "GetProperty failed: %08lx\n", hr);

        hr = IDirectInputDevice_EnumObjects(device, &obj_cb, NULL, DIDFT_ALL);
        if(hr != S_OK)
            fprintf(f, "EnumObjects failed: %08lx\n", hr);

        hr = IDirectInputDevice_SetCooperativeLevel(device, NULL, DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);
        if(hr != S_OK)
            fprintf(f, "SetCooperativeLevel failed: %08lx\n", hr);

        hr = IDirectInputDevice_SetDataFormat(device, &c_dfDIJoystick2);
        if(hr != S_OK)
            fprintf(f, "SetDataFormat failed: %08lx\n", hr);

        hr = IDirectInputDevice_Acquire(device);
        if(hr != S_OK)
            fprintf(f, "Acquire failed: %08lx\n", hr);

        //if( IsXInputDevice( &ddi->guidProduct ) )
         //   fprintf(f, "Yes, it's an XInput device\n");

        while(1)
        {
            hr = IDirectInputDevice_GetDeviceState(device, sizeof(dat), &dat);
            if(hr != S_OK)
                fprintf(f, "GetDeviceState failed: %08lx\n", hr);
            else{
                if(memcmp(dat.rgdwPOV, last.rgdwPOV, sizeof(last.rgdwPOV)) ||
                        memcmp(dat.rgbButtons, last.rgbButtons, sizeof(last.rgbButtons)))
                {
                    fprintf(f, "lX: %d, lY: %d, lZ: %d, lRx: %d, lRy: %d, lRz: %d, sl1: %d, sl2: %d POV: %d, Buttons:",
                            dat.lX,
                            dat.lY,
                            dat.lZ,
                            dat.lRx,
                            dat.lRy,
                            dat.lRz,
                            dat.rglSlider[0],
                            dat.rglSlider[1],
                            dat.rgdwPOV[0]);
                    for(i = 0; i < 128; ++i)
                        if(dat.rgbButtons[i])
                            fprintf(f, " %d", i);
                    fprintf(f, "\n");
#if 0
                    fprintf(f, "lX: % 8ld, lY: % 8ld, lZ: % 8ld, lRx: % 8ld, lRy: % 8ld, lRz: % 8ld\n",
                            dat.lX,
                            dat.lY,
                            dat.lZ,
                            dat.lRx,
                            dat.lRy,
                            dat.lRz);
#endif
                }
                memcpy(&last, &dat, sizeof(last));
            }
            Sleep(10);
        }
    }else
        fprintf(f, "CreateDevice failed: %08lx\n", hr);

    return DIENUM_CONTINUE;
}

int main(int argc, char **argv)
{
    HRESULT hr;
    DWORD dr;
//    XINPUT_STATE state;

    if(argc > 1)
        f = fopen(argv[1], "w");
    else
        f = stdout;

#if 0
    XInputEnable(TRUE);

    dr = XInputGetState(0, &state);
    fprintf(f, "XInput device 0 is %s\n",
            dr == ERROR_SUCCESS ? "connected" : "not connected");

    XInputEnable(FALSE);
#endif

    hr = DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, &IID_IDirectInput8A, (void**)&di, NULL);
    if(hr != S_OK)
    {
        fprintf(f, "DirectInputCreate: %08lx\n", hr);
        return 1;
    }

    hr = IDirectInput_EnumDevices(di, DI8DEVCLASS_GAMECTRL, &di_cb, NULL, DIEDFL_ATTACHEDONLY);
    if(hr != S_OK)
    {
        fprintf(f, "EnumDevices: %08lx\n", hr);
        return 1;
    }

    fprintf(f, "Done.\n");

//    while(1);

    return 0;
}
