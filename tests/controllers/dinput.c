#include <windows.h>

#define COBJMACROS
#include <xinput.h>
#include <initguid.h>
#include <dinput.h>
#include <wbemidl.h>
#include <wbemdisp.h>
#include <oleauto.h>
#include <unknwn.h>
#include "ddk/hidsdi.h"
#include "ddk/hidclass.h"

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

static IDirectInput8 *di;
static IDirectInputDevice8 *device;
static FILE *f;

static const char *debugstr_guid(const GUID *id)
{
    static char s[256];
    sprintf(s, "{%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x}",
                             id->Data1, id->Data2, id->Data3,
                             id->Data4[0], id->Data4[1], id->Data4[2], id->Data4[3],
                             id->Data4[4], id->Data4[5], id->Data4[6], id->Data4[7]);
    return s;
}

BOOL CALLBACK obj_cb(const DIDEVICEOBJECTINSTANCEA *obj, void *user)
{
    HRESULT hr;

    fprintf(f, "got object:\n");
    fprintf(f, "\toffs: %ld\n", obj->dwOfs);
    fprintf(f, "\ttype: 0x%08lx\n", obj->dwType);
    fprintf(f, "\tflags: 0x%08lx\n", obj->dwFlags);
    fprintf(f, "\tname: %s\n", obj->tszName);
    fprintf(f, "\tguidtype: %s\n", debugstr_guid(&obj->guidType));

    if(obj->dwType & DIDFT_AXIS){
        DIPROPRANGE range;

        range.diph.dwSize = sizeof(range);
        range.diph.dwHeaderSize = sizeof(range.diph);
        range.diph.dwObj = obj->dwOfs;
        range.diph.dwHow = DIPH_BYOFFSET;
        range.lMin = 0;
        range.lMax = 65535;

        hr = IDirectInputDevice8_SetProperty(device, DIPROP_RANGE, &range.diph);
        fprintf(f, "SetProperty: %08x\n", hr);

    }

    return DIENUM_CONTINUE;
}

BOOL CALLBACK eff_cb(const DIEFFECTINFOA *effect_info, void *user)
{
    fprintf(f, "got effect:\n");
    fprintf(f, "\tguid: %s\n", debugstr_guid(&effect_info->guid));
    fprintf(f, "\tdwEffType: 0x%x\n", effect_info->dwEffType);
    fprintf(f, "\tdwStaticParams: 0x%x\n", effect_info->dwStaticParams);
    fprintf(f, "\tdwDynamicParams: 0x%x\n", effect_info->dwDynamicParams);
    fprintf(f, "\tname: %s\n", effect_info->tszName);
}

BOOL CALLBACK di_cb(const DIDEVICEINSTANCEA *ddi, void *user)
{
    HRESULT hr;
    DIPROPDWORD prop;
    DIPROPGUIDANDPATH gnp;
    HANDLE hid;
    HIDD_ATTRIBUTES attr;
    DIDEVCAPS devcaps;
    WCHAR string[512];
    char stringA[512];

    fprintf(f, "got size: 0x%x\n", ddi->dwSize);
    fprintf(f, "got instance guid: %s\n", debugstr_guid(&ddi->guidInstance));
    fprintf(f, "got product guid: %s\n", debugstr_guid(&ddi->guidProduct));
    fprintf(f, "got devtype: 0x%x\n", ddi->dwDevType);
    fprintf(f, "got instance name: %s\n", ddi->tszInstanceName);
    fprintf(f, "got product name: %s\n", ddi->tszProductName);
    fprintf(f, "got ffdriver: %s\n", debugstr_guid(&ddi->guidFFDriver));
    fprintf(f, "got usage page/usage: 0x%hx, 0x%hx\n", ddi->wUsagePage, ddi->wUsage);
    fflush(f);

    hr = IDirectInput8_CreateDevice(di, &ddi->guidInstance, &device, NULL);
    if(hr == S_OK)
    {
        memset(&devcaps, 0, sizeof(devcaps));
        devcaps.dwSize = sizeof(devcaps);
        hr = IDirectInputDevice8_GetCapabilities(device, &devcaps);
        if(hr == S_OK)
        {
            fprintf(f, "got caps.dwFlags: 0x%x\n", devcaps.dwFlags);
            fprintf(f, "got caps.dwDevType: 0x%x\n", devcaps.dwDevType);
            fprintf(f, "got caps.dwAxes: 0x%x\n", devcaps.dwAxes);
            fprintf(f, "got caps.dwButtons: 0x%x\n", devcaps.dwButtons);
            fprintf(f, "got caps.dwPOVs: 0x%x\n", devcaps.dwPOVs);
            fprintf(f, "got caps.dwFFSamplePeriod: 0x%x\n", devcaps.dwFFSamplePeriod);
            fprintf(f, "got caps.dwFFMinTimeResolution: 0x%x\n", devcaps.dwFFMinTimeResolution);
            fprintf(f, "got caps.dwFirmwareRevision: 0x%x\n", devcaps.dwFirmwareRevision);
            fprintf(f, "got caps.dwHardwareRevision: 0x%x\n", devcaps.dwHardwareRevision);
            fprintf(f, "got caps.dwFFDriverVersion: 0x%x\n", devcaps.dwFFDriverVersion);
        }else
            fprintf(f, "GetCapabilities failed\n");

#if 0
        memset(&gnp, 0, sizeof(gnp));
        gnp.diph.dwSize = sizeof(gnp);
        gnp.diph.dwHeaderSize = sizeof(gnp.diph);
        gnp.diph.dwObj = 0;
        gnp.diph.dwHow = DIPH_DEVICE;

        hr = IDirectInputDevice8_GetProperty(device, DIPROP_GUIDANDPATH, &gnp.diph);
        if(hr == S_OK)
        {
            WideCharToMultiByte(CP_ACP, 0, gnp.wszPath, -1, stringA, sizeof(stringA), 0, NULL);
            fprintf(f, "Got GNP path: %s\n", stringA);
        }
        else
            fprintf(f, "GetProperty(GNP) failed: %08lx\n", hr);

        hid = CreateFileW(gnp.wszPath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
        if(hid != INVALID_HANDLE_VALUE)
        {
            HidD_GetAttributes(hid, &attr);
            fprintf(f, "got vid 0x%hx, pid 0x%hx, ver 0x%hx\n", attr.VendorID, attr.ProductID, attr.VersionNumber);

            memset(string, 0, sizeof(string));
            if(HidD_GetManufacturerString(hid, string, sizeof(string)))
            {
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got manufacturer string: %s\n", stringA);
            }else
                fprintf(f, "GetManufacturerString failed: 0x%x\n", GetLastError());

            memset(string, 0, sizeof(string));
            if(HidD_GetProductString(hid, string, sizeof(string)))
            {
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got product string: %s\n", stringA);
            }else
                fprintf(f, "GetProductString failed: 0x%x\n", GetLastError());

            memset(string, 0, sizeof(string));
            if(HidD_GetSerialNumberString(hid, string, sizeof(string))){
                WideCharToMultiByte(CP_ACP, 0, string, -1, stringA, sizeof(stringA), 0, NULL);
                fprintf(f, "got serial number string: %s\n", stringA);
            }else
                fprintf(f, "GetSerialNumberString failed: 0x%x\n", GetLastError());

            CloseHandle(hid);
        }else
            fprintf(f, "opening file path failed\n");
#endif

        memset(&prop, 0, sizeof(prop));
        prop.diph.dwSize = sizeof(prop);
        prop.diph.dwHeaderSize = sizeof(prop.diph);
        prop.diph.dwObj = 0;
        prop.diph.dwHow = DIPH_DEVICE;

        hr = IDirectInputDevice8_GetProperty(device, DIPROP_VIDPID, &prop.diph);
        if(hr == S_OK)
            fprintf(f, "Got vidpid: %08lx\n", prop.dwData);
        else
            fprintf(f, "GetProperty failed: %08lx\n", hr);

        hr = IDirectInputDevice8_SetCooperativeLevel(device, NULL, DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);
        if(hr != S_OK)
            fprintf(f, "SetCooperativeLevel failed: %08lx\n", hr);

        hr = IDirectInputDevice8_SetDataFormat(device, &c_dfDIJoystick2);
        if(hr != S_OK)
            fprintf(f, "SetDataFormat failed: %08lx\n", hr);

        hr = IDirectInputDevice8_EnumObjects(device, &obj_cb, NULL, DIDFT_ALL);
        if(hr != S_OK)
            fprintf(f, "EnumObjects failed: %08lx\n", hr);

        fprintf(f, "enum effects\n");
        hr = IDirectInputDevice8_EnumEffects(device, &eff_cb, NULL, 1);
        if(hr != S_OK)
            fprintf(f, "EnumEffects failed: %08lx\n", hr);

        hr = IDirectInputDevice8_Acquire(device);
        if(hr != S_OK)
            fprintf(f, "Acquire failed: %08lx\n", hr);

        return DIENUM_STOP;
    }else
        fprintf(f, "CreateDevice failed: %08lx\n", hr);

    return DIENUM_CONTINUE;
}

static void read_device(void)
{
    DIJOYSTATE2 dat, last;
    HRESULT hr = S_OK;
    int i;

    while(device)// && hr == S_OK)
    {
        hr = IDirectInputDevice8_GetDeviceState(device, sizeof(dat), &dat);
        if(hr != S_OK)
        {
            fprintf(f, "GetDeviceState failed: %08lx\n", hr);
            hr = IDirectInputDevice8_Acquire(device);
            fprintf(f, "acquire gave: %08x\n", hr);

        //    IDirectInputDevice8_Release(device);
         //   device = NULL;
        }
        else{
            if(memcmp(dat.rgdwPOV, last.rgdwPOV, sizeof(last.rgdwPOV)) ||
                    memcmp(dat.rgbButtons, last.rgbButtons, sizeof(last.rgbButtons)))
            {
                fprintf(f, "XYZ: {%d, %d, %d}\n", dat.lX, dat.lY, dat.lZ);
                fprintf(f, "R-XYZ: {%d, %d, %d}\n", dat.lRx, dat.lRy, dat.lRz);
                fprintf(f, "Slider[2]: {%d, %d}\n", dat.rglSlider[0], dat.rglSlider[1]);
                fprintf(f, "POV[4]: {0x%x, 0x%x, 0x%x, 0x%x}\n",
                        dat.rgdwPOV[0],
                        dat.rgdwPOV[1],
                        dat.rgdwPOV[2],
                        dat.rgdwPOV[3]);
                fprintf(f, "V-XYZ: {%d, %d, %d}\n", dat.lVX, dat.lVY, dat.lVZ);
                fprintf(f, "VR-XYZ: {%d, %d, %d}\n", dat.lVRx, dat.lVRy, dat.lVRz);
                fprintf(f, "VSlider[2]: {%d, %d}\n", dat.rglVSlider[0], dat.rglVSlider[1]);
                fprintf(f, "A-XYZ: {%d, %d, %d}\n", dat.lAX, dat.lAY, dat.lAZ);
                fprintf(f, "AR-XYZ: {%d, %d, %d}\n", dat.lARx, dat.lARy, dat.lARz);
                fprintf(f, "ASlider[2]: {%d, %d}\n", dat.rglASlider[0], dat.rglASlider[1]);
                fprintf(f, "F-XYZ: {%d, %d, %d}\n", dat.lFX, dat.lFY, dat.lFZ);
                fprintf(f, "FR-XYZ: {%d, %d, %d}\n", dat.lFRx, dat.lFRy, dat.lFRz);
                fprintf(f, "FSlider[2]: {%d, %d}\n", dat.rglFSlider[0], dat.rglFSlider[1]);

                fprintf(f, "Buttons: ");
                for(i = 0; i < 128; ++i)
                    if(dat.rgbButtons[i])
                        fprintf(f, " %d", i);
                fprintf(f, "\n");
#if 0
                fprintf(f, "lX: %d, lY: %d, lZ: %d, lRx: %d, lRy: %d, lRz: %d, sl1: %d, sl2: %d POV: %d, Buttons:",
                        dat.lX,
                        dat.lY,
                        dat.lZ,
                        dat.lRx,
                        dat.lRy,
                        dat.lRz,
                        dat.rglSlider[0],
                        dat.rglSlider[1],
                        dat.rgdwPOV[0]);
                for(i = 0; i < 128; ++i)
                    if(dat.rgbButtons[i])
                        fprintf(f, " %d", i);
                fprintf(f, "\n");
#endif
#if 0
                fprintf(f, "lX: % 8ld, lY: % 8ld, lZ: % 8ld, lRx: % 8ld, lRy: % 8ld, lRz: % 8ld\n",
                        dat.lX,
                        dat.lY,
                        dat.lZ,
                        dat.lRx,
                        dat.lRy,
                        dat.lRz);
#endif
            }
            memcpy(&last, &dat, sizeof(last));
        }
        Sleep(10);
    }
}

int main(int argc, char **argv)
{
    HRESULT hr;
    DWORD dr;
    GUID null_guid = {0};

    if(argc > 1)
        f = fopen(argv[1], "w");
    else
        f = stdout;

    hr = DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, &IID_IDirectInput8A, (void**)&di, NULL);
    if(hr != S_OK)
    {
        fprintf(f, "DirectInputCreate: %08lx\n", hr);
        return 1;
    }

    hr = IDirectInput8_GetDeviceStatus(di, &null_guid);
    fprintf(f, "NULL guid gave: %08lx\n", hr);

        hr = IDirectInput8_EnumDevices(di, DI8DEVCLASS_GAMECTRL, &di_cb, NULL, DIEDFL_ATTACHEDONLY);
        if(hr != S_OK)
        {
            fprintf(f, "EnumDevices: %08lx\n", hr);
            return 1;
        }

        fprintf(f, "DIJOYSTATE2 size: %u\n", (int)sizeof(DIJOYSTATE2));
    while(1){

        read_device();

        Sleep(100);
    }

    fprintf(f, "Done.\n");

    return 0;
}
