#!/usr/bin/env python2

import PIL.Image as Image, PIL.ImageDraw as ImageDraw
import sys
from decimal import Decimal

# Useful for rendering calls to a ID2D1SimplifiedGeometrySink
# interface, for example from the outline renderer in dwrite.

# input file format is any number of lines of:
# BeginFigure (MoveTo):
#   M x1 y1
# AddLines (LineTo):
#   L x1 y1
# AddBeziers (BezierTo):
#   B x1 y1 x2 y2 x3 y3
# EndFigure:
#   E
# Lines that don't start with a single M, L, B, or E character
# are ignored.

# e.g. an input file would be:
# M 10.0 10.0
# L 100.0 100.0
# E

if len(sys.argv) != 3:
    print "Usage:", sys.argv[0], "<file>", "<output file>"
    exit(1)

data = []
with open(sys.argv[1]) as f:
    for line in f:
        strs = line.strip().split(" ")
        if strs[0] == 'M' or strs[0] == 'L':
            data.append((strs[0], [int(Decimal(strs[1]) / 1000), int(Decimal(strs[2]) / 1000)]))
        elif strs[0] == 'B':
            data.append((strs[0], [int(Decimal(strs[1]) / 1000), int(Decimal(strs[2]) / 1000), \
                    int(Decimal(strs[3]) / 1000), int(Decimal(strs[4]) / 1000), \
                    int(Decimal(strs[5]) / 1000), int(Decimal(strs[6]) / 1000)]))
        elif strs[0] == 'E':
            data.append((strs[0]))

maxX = maxY = 0
minX = minY = 0
for l in data:
    if l[0] == 'M' or l[0] == 'L':
        points = l[1]
        if points[0] > maxX:
            maxX = points[0]
        if points[1] > maxY:
            maxY = points[1]

        if points[0] < minX:
            minX = points[0]
        if points[1] < minY:
            minY = points[1]

    if l[0] == 'B':
        points = l[1]
        if points[0] > maxX:
            maxX = points[0]
        if points[1] > maxY:
            maxY = points[1]
        if points[2] > maxX:
            maxX = points[2]
        if points[3] > maxY:
            maxY = points[3]
        if points[4] > maxX:
            maxX = points[4]
        if points[5] > maxY:
            maxY = points[5]

        if points[0] < minX:
            minX = points[0]
        if points[1] < minY:
            minY = points[1]
        if points[2] < minX:
            minX = points[2]
        if points[3] < minY:
            minY = points[3]
        if points[4] < minX:
            minX = points[4]
        if points[5] < minY:
            minY = points[5]

if minX < 0:
    dX = minX * -1 + 100
    maxX = maxX + dX
else:
    dX = 100
for l in data:
    if l[0] == 'M' or l[0] == 'L':
        points = l[1]
        points[0] = points[0] + dX

    if l[0] == 'B':
        points = l[1]
        points[0] = points[0] + dX
        points[2] = points[2] + dX
        points[4] = points[4] + dX

if minY < 0:
    dY = minY * -1 + 100
    maxY = maxY + dY
else:
    dY = 100
for l in data:
    if l[0] == 'M' or l[0] == 'L':
        points = l[1]
        points[1] = points[1] + dY

    if l[0] == 'B':
        points = l[1]
        points[1] = points[1] + dY
        points[3] = points[3] + dY
        points[5] = points[5] + dY

img = Image.new("RGB", (maxX + 200, maxY + 200))
draw = ImageDraw.Draw(img)

cursorX = 0
cursorY = 0

BEZ_STEPS = 100

def drawtext(prefix):
    draw.ellipse((cursorX-10, cursorY-10, cursorX+10, cursorY+10), fill=fill)
    draw.text((cursorX+8 + 6 * (i % 8), cursorY+8 + 6 * (i % 8)), prefix + str(i))

for i in range(0, len(data)):
    fillt = int(0xff * (i / float(len(data))))
    fill = fillt << 16 | fillt << 8 | fillt
    l = data[i]
    if l[0] == 'M':
        points = l[1]
        cursorX = points[0]
        cursorY = points[1]
        drawtext("M:")

    elif l[0] == 'L':
        points = l[1]
        draw.line((cursorX, cursorY, points[0], points[1]))
        cursorX = points[0]
        cursorY = points[1]
        drawtext("L:")

    elif l[0] == 'B':
        points = l[1]

        pX = cursorX
        pY = cursorY

        for z in range(0, BEZ_STEPS):
            t = z / float(BEZ_STEPS)
            toX = (1 - t) * (1 - t) * (1 - t) * cursorX + \
                    3 * (1 - t) * (1 - t) * t * points[0] + \
                    3 * (1 - t) * t * t * points[2] + \
                    t * t * t * points[4]

            toY = (1 - t) * (1 - t) * (1 - t) * cursorY + \
                    3 * (1 - t) * (1 - t) * t * points[1] + \
                    3 * (1 - t) * t * t * points[3] + \
                    t * t * t * points[5]

            draw.line((pX, pY, toX, toY))

            pX = toX
            pY = toY

        cursorX = points[4]
        cursorY = points[5]

        drawtext("B:")

    elif l[0] == 'E':
        drawtext("E:")

img.save(sys.argv[2])
