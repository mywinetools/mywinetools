" Usage:
"
" Copy the VTBL spec from the C header into the C file twice:
"
"    /*** IUnknown methods ***/
"    HRESULT (STDMETHODCALLTYPE *QueryInterface)(
"    ...
"
" Mark before and after the first block with @@@@ and :::: respectively.
"
" Run :call MakeFunctions()
"
" Mark before and after the second block with @@@@ and ::::.
"
" Run :call MakeVtbl()
"
" Do these substitutions:
"
" :%s/\CSWAPPREFIX/object/
" :%s/\CSWAPIFACE/IObject/
" :%s/\CSWAPSTRUCT/ObjectImpl/
"
" Clean up formatting, fix AddRef, Release return values, add IObjectVtbl
" declaration, etc.

function! MakeFunctions()
    :/@@@@/+1,/::::/-1s/^.*\/\*.*$//
    :/@@@@/+1,/::::/-1s/^\s*\(.*\) (STDMETHODCALLTYPE \*/static \1 WINAPI SWAPPREFIX_/
    :/@@@@/+1,/::::/-1s/)(/(/
    :/@@@@/+1,/::::/-1s/This/iface/
    :/@@@@/+1,/::::/-1s/;/\r{\r    SWAPSTRUCT *This = impl_from_SWAPIFACE(iface);\r    FIXME("%p\\n", This);\r    return E_NOTIMPL;\r}/
    :%g/@@@@/d
    :%g/::::/d
endfunction

function! MakeVtbl()
    :/@@@@/+1,/::::/-1s/^.*[^\(]$\n//
    :/@@@@/+1,/::::/-1s/^$\n//g
    :/@@@@/+1,/::::/-1s/^.*\*/    SWAPPREFIX_/
    :/@@@@/+1,/::::/-1s/)(/,/
    :%g/@@@@/d
    :%g/::::/d
endfunction
